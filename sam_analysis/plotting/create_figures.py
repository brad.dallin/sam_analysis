"""
k_fold_cross_validation.py
This script contains code to run k-fold cross validation

CREATED ON: 01/05/2021

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS AND SYS
import os
## IMPORT NUMPY
import numpy as np  # Used to do math functions

## FUNCTION TO SAVE AND LOAD PICKLE FILES
from sam_analysis.core.pickles import load_pkl
## IMPORT CHECK SERVER PATH
from sam_analysis.core.check_tools import check_server_path
                                           
#%%
##############################################################################
## MAIN SCRIPT
##############################################################################
if __name__ == "__main__":
    ## WORKING DIRECTORY
    working_dir = r"/mnt/r/python_projects/sam_analysis/sam_analysis/raw_data"
    working_dir = check_server_path( working_dir )
    
    ## DATA FILES
    data_pkl = [ 
                 r"unbiased_regression_data.pkl",
#                 r"unbiased_regression_data_wc.pkl"
                 r"indus_regression_data.pkl"
                 ]
    
    ## CREATE DATA STRUCTURE
    data = {}
    
    ## LOAD DATA FILES
    for pkl in data_pkl:
        ## LOAD DATA
        path_pkl = os.path.join( working_dir, pkl )
        raw_data = load_pkl( path_pkl )
                
        ## LOOP THROUGH SAM TYPE
        for key in raw_data.keys():
            if key not in data.keys():
                data[key] = raw_data[key]
            else:
                data[key].update( raw_data[key] )
        