# -*- coding: utf-8 -*-
"""
plotting_functions.py
script contains plotting functions for SAM simulation data

CREATED ON: 12/14/2020

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS
import os
## IMPORT NUMPY
import numpy as np
## IMPORT MATPLOTLIB
from matplotlib import cm
import matplotlib.pyplot as plt

## IMPORT SEABORN
import seaborn as sns

## FUNCTION TO SAVE AND LOAD PICKLE FILES
from sam_analysis.core.pickles import load_pkl
## IMPORT JACS PLOT STYLE
from sam_analysis.plotting.jacs_single import JACS

##############################################################################
## PLOTTING FUNCTIONS
##############################################################################
## CREATE LINE PLOT
def plot_line( x, y, 
               yerr         = [],
               title        = None,
               xlabel       = None,
               ylabel       = None,
               xticks       = [],
               yticks       = [],
               markers      = None,
               colors       = None,
               line_labels  = None,
               legend_cols  = 1,
               fig_path     = None, ):
    ## PRINT
    print( "\n--- CREATING LINE PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )

    ## MAKE NONE LIST
    if line_labels is None:
        line_labels = len(y) * [ None ]

    ## GET MARKERS
    if markers is None:
        markers = len(y) * [ "s" ]

    ## SET X AND Y LABELS
    if xlabel is not None:
        ax.set_xlabel( xlabel )
    if ylabel is not None:
        ax.set_ylabel( ylabel )
    
    ## PUT Y IN LIST IF NOT
    if type(y) is not list:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(yerr) < 1:
            yerr = np.zeros_like(y)
        ## PUT Y DATA IN LISTS
        y = [ y ]
        yerr = [ yerr ]
    else:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(yerr) < 1:
            yerr = []
            for yy in y:
                yerr.append( np.zeros_like(yy) )
    
    ## GET COLOR MAP
    if colors is None:
        colors = [ plot_details.colormap(ii) for ii in np.linspace( 0.0, 1.0, len(y) ) ]

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x, y, yerr )):
        ## PLOT LINES
        plt.plot( xx, yy,
                  linestyle  = '-',
                  linewidth  = 1.5,
                  color      = colors[ii],
                  label      = line_labels[ii] )                
        ## PLOT SHADED ERROR
        plt.plot( xx,
                  yy + yyerr,
                  linestyle = '-',
                  linewidth = 1.0,
                  color     = colors[ii] )
        plt.plot( xx,
                  yy - yyerr,
                  linestyle = '-',
                  linewidth = 1.0,
                  color     = colors[ii] )
        plt.fill_between( xx,
                          yy + yyerr,
                          yy - yyerr,
                          color = colors[ii],
                          alpha = 0.5, )

    ## SET X AND Y TICKS
    if len(xticks) > 1:
        x_min   = xticks[0]
        x_max   = xticks[1]
        x_diff  = xticks[2]
        x_lower = x_min - 0.5*x_diff
        x_upper = x_max + 0.5*x_diff
        ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
        ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
        ax.set_xlim( x_lower, x_upper )
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )

    ## ADD LEGEND
    if len(line_labels) > 1:
        ax.legend( loc = 'best', ncol = legend_cols, fontsize = 6 )
        
    fig.set_size_inches( plot_details.width, plot_details.height ) # 4:3 aspect ratio
    # fig.set_size_inches( 2*plot_details.width, 2*plot_details.height ) # 4:3 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE LINE PLOT WITH POINTS
def plot_line_points( x, y, 
                      xerr         = [],
                      yerr         = [],
                      title        = None,
                      xlabel       = None,
                      ylabel       = None,
                      xticks       = [],
                      yticks       = [],
                      fit_line     = None,
                      markers      = None,
                      colors       = None,
                      point_labels = None,
                      legend_cols  = 1,
                      fig_path     = None, ):
    ## PRINT
    print( "\n--- CREATING LINE PLOT WITH POINTS ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )

    ## MAKE NONE LIST
    if point_labels is None:
        point_labels = len(y) * [ None ]

    ## GET MARKERS
    if markers is None:
        markers = len(y) * [ "s" ]

    ## SET X AND Y LABELS
    if xlabel is not None:
        ax.set_xlabel( xlabel )
    if ylabel is not None:
        ax.set_ylabel( ylabel )
    
    ## PUT Y IN LIST IF NOT
    if type(y) is not list:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(xerr) < 1:
            xerr = np.zeros_like(x)
        if len(yerr) < 1:
            yerr = np.zeros_like(y)
        ## PUT Y DATA IN LISTS
        y = [ y ]
        yerr = [ yerr ]
    else:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(xerr) < 1:
            xerr = []
            for xx in x:
                xerr.append( np.zeros_like(xx) )
        if len(yerr) < 1:
            yerr = []
            for yy in y:
                yerr.append( np.zeros_like(yy) )
    
    ## GET COLOR MAP
    if colors is None:
        colors = [ plot_details.colormap(ii) for ii in np.linspace( 0.0, 1.0, len(y) ) ]

    ## LOOP THROUGH Y TYPES
    for ii, (xx, xxerr, yy, yyerr) in enumerate(zip( x, xerr, y, yerr )):
        ## PLOT LINES
        plt.plot( xx, yy,
                  marker     = markers[ii],
                  markersize = 6,
                  linestyle  = "None",
                  linewidth  = 1.5,
                  color      = colors[ii],
                  label      = point_labels[ii] )                
        ## PLOT POINTS WITH ERROR BARS
        ax.errorbar( xx, yy,
                     xerr       = xxerr,
                     yerr       = yyerr,
                     linestyle  = "None",
                     marker     = markers[ii],
                     markersize = 6.,
                     color      = colors[ii],
                     ecolor     = colors[ii],
                     elinewidth = 0.5, 
                     capsize    = 2., )
        # ## PLOT WITH SCATTER FOR BETTER LEGEND
        # ax.scatter( [], [],
        #             marker = markers[ii],
        #             s      = 6**2,
        #             color  = colors[ii],
        #             label  = point_labels[ii] )

    ## ADD FITTED LINE
    if fit_line is not None:
        for ii, fit in enumerate(fit_line):
            ## PLOT FIT LINES
            ax.plot( fit[0],
                     fit[1],
                     linestyle = ":",
                     linewidth = 1.5,
                     color = colors[ii], )

    ## SET X AND Y TICKS
    if len(xticks) > 1:
        x_min   = xticks[0]
        x_max   = xticks[1]
        x_diff  = xticks[2]
        x_lower = x_min - 0.5*x_diff
        x_upper = x_max + 0.5*x_diff
        ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
        ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
        ax.set_xlim( x_lower, x_upper )
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )

    ## ADD LEGEND
    if len(point_labels) > 1:
        ax.legend( loc = 'best', ncol = legend_cols, fontsize = 6 )
        
    fig.set_size_inches( plot_details.width, plot_details.height ) # 4:3 aspect ratio
    # fig.set_size_inches( 2*plot_details.width, 2*plot_details.height ) # 4:3 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE MULIT LINE
def plot_multi_line( x, y,
                     yerr        = [],
                     plot_labels = None,
                     plot_dims   = [ 1, 1 ],
                     figsize     = None,
                     xlabel      = None,
                     ylabel      = None,
                     xticks      = [],
                     yticks      = [],
                     fig_path    = None, ):
    def _trim_axs( axs, N ):
        ## FLATTEN AXES
        axs = axs.flat

        ## LOOP THROUGH AXES
        for ax in axs[N:]:
            ax.remove()
        
        ## RETURN
        return axs[:N]

    ## PRINT
    print( "\n--- CREATING LINE PLOTS ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    axs = plt.figure( figsize = figsize, constrained_layout = True ).subplots( plot_dims[0], plot_dims[1] )
    axs = _trim_axs( axs, int( plot_dims[0] * plot_dims[1]) )
    
    ## LOOP THROUGH PLOTS
    cc = 0
    for xx, yy, yyerr, ax, label in zip( x, y, yerr, axs, plot_labels ):
        ## SET TITLE
        if plot_labels is not None:
            ax.set_title( label, pad = 0 )

        ## SET X AND Y LABELS
        if xlabel is not None:
            ax.set_xlabel( xlabel )
        if ylabel is not None:
            ax.set_ylabel( ylabel )
        
        ## SET ERR TO ZERO IF EMPTY
        if len(yerr) < 1:
            yerr = []
            for yy in y:
                yerr.append( np.zeros_like(yy) )

        ## PLOT LINES
        ax.plot( xx, yy,
                 linestyle  = '-',
                 linewidth  = 1.5,
                 color      = "black", )
        ## PLOT SHADED ERROR
        ax.plot( xx,
                 yy + yyerr,
                 linestyle = ':',
                 linewidth = 1.0,
                 color     = "black" )
        ax.plot( xx,
                 yy - yyerr,
                 linestyle = ':',
                 linewidth = 1.0,
                 color     = "black" )
        ax.fill_between( xx,
                         yy + yyerr,
                         yy - yyerr,
                         color = "black",
                         alpha = 0.3, )

        ## SET X AND Y TICKS
        if len(xticks[cc]) > 1:
            x_min   = xticks[cc][0]
            x_max   = xticks[cc][1]
            x_diff  = xticks[cc][2]
            x_lower = x_min - 0.5*x_diff
            x_upper = x_max + 0.5*x_diff
            ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
            ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
            ax.set_xlim( x_lower, x_upper )
        
        if len(yticks[cc]) > 1:
            y_min   = yticks[cc][0]
            y_max   = yticks[cc][1]
            y_diff  = yticks[cc][2]
            y_lower = y_min - 0.5*y_diff
            y_upper = y_max + 0.5*y_diff
            ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
            ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
            ax.set_ylim( y_lower, y_upper )
        
        ## UPDATE COUNTER
        cc += 1
        
    # plt.set_size_inches( 6.5, 6.5*0.60 ) # 4:3 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        plt.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        plt.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE BAR PLOT
def plot_bar( x, y,
              yerr       = [],
              title      = None,
              xlabel     = r"x label",
              ylabel     = r"y label",
              xticks     = [],
              yticks     = [],
              bar_labels = None,
              colors     = None,
              fig_path   = None, ):
    ## PRINT
    print( "\n--- CREATING BAR PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    # fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    # fig.subplots_adjust( left = 0.15, bottom = 0.30, right = 0.85, top = 0.98 )
    fig.subplots_adjust( left = 0.15, bottom = 0.45, right = 0.95, top = 0.98 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )
    
    ## SET X AND Y LABELS
    if xlabel is not None and type(xlabel) is not list:
        ax.set_xlabel( xlabel )
    if ylabel is not None:
        ax.set_ylabel( ylabel )

    ## MAKE NONE LIST
    if bar_labels is None:
        bar_labels = len(y) * [ None ]

    ## PUT Y IN LIST IF NOT
    if type(y) is not list:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(yerr) < 1:
            yerr = np.zeros_like(y)
        ## PUT Y DATA IN LISTS
        y = [ y ]
        yerr = [ yerr ]
    else:
        ## GENERATE ZEROS FOR ERR IF NONE
        if len(yerr) < 1:
            yerr = []
            for yy in y:
                yerr.append( np.zeros_like(yy) )

    ## GET COLOR MAP
    if colors is None:
        if type(y) is not list:
            colors = [ "grey" ]
        else:
            colors = [ plot_details.colormap(ii) for ii in np.linspace( 0.0, 1.0, len(y) ) ]

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x, y, yerr )): 
        ## PLOT BARS
        plt.bar( xx, yy,
                 linestyle = "None",
                 color     = colors[ii],
                 edgecolor = "black", 
                 linewidth = 0.5,
                 yerr      = yyerr,
                 ecolor    = "black",
                 capsize   = 2.0,
                 label     = bar_labels[ii] )
         
    ## SET X AND Y TICKS
    if len(xticks) > 1:
        ## PLOT LINE AT Y=0
        plt.plot( [ xticks[0]-1, xticks[1]+1 ], 
                [ 0, 0 ],
                linestyle = "-",
                linewidth = 0.5,
                color = "black", )

        x_min   = xticks[0]
        x_max   = xticks[1]
        x_diff  = xticks[2]
        x_lower = x_min - 0.5*x_diff
        x_upper = x_max + 0.5*x_diff
        ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
        ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
        ax.set_xlim( x_lower, x_upper )
    else:
        ## PLOT LINE AT Y=0
        plt.plot( [-1, len(xlabel)+1 ], 
                [ 0, 0 ],
                linestyle = "-",
                linewidth = 0.5,
                color = "black", )

        ## GET XTICK WIDTH
        x_tick = 1
        ## GET X MAJOR TICKS
        x_ticks = np.arange( 0., len(xlabel), 1 )
        ## GET X MINOR TICKS
        x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
        ## SET X TICKS
        ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
        ax.set_xticks( x_ticks, minor = False )       # sets major ticks
        ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
        ax.set_xticklabels( xlabel, rotation = 90 ) # sets tick labels        
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )
    
    ## ADD LEGEND
    if len(y) > 1:
        ax.legend( loc = 'best', ncol = 1, fontsize = 6 )

    fig.set_size_inches( plot_details.width, plot_details.width ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE BAR PLOT
def plot_stacked_bar( x, y_top, y_bottom,
                      yerr_top    = [],
                      yerr_bottom = [],
                      title       = None,
                      ylabel      = r"y label",
                      xticks      = [],
                      yticks      = [],
                      bar_labels  = None,
                      colors      = None,
                      fig_path    = None, ):
    ## PRINT
    print( "\n--- CREATING STACKED BAR PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    # fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    # fig.subplots_adjust( left = 0.15, bottom = 0.30, right = 0.85, top = 0.98 )
    fig.subplots_adjust( left = 0.15, bottom = 0.45, right = 0.95, top = 0.98 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )
    
    ## SET Y LABEL
    if ylabel is not None:
        ax.set_ylabel( ylabel )

    ## GENERATE ZEROS FOR ERR IF NONE
    if len(yerr_top) < 1:
        yerr_top = np.zeros_like(y_top)

    ## GENERATE ZEROS FOR ERR IF NONE
    if len(yerr_bottom) < 1:
        yerr_bottom = np.zeros_like(y_bottom)

    ## GET COLOR MAP
    if colors is None:
        colors = [ "tomato", "slateblue" ]

    ## GET BAR LABELS
    if bar_labels is None:
        bar_labels = [ None, None ]

    ## PLOT BOTTOM BARS
    plt.bar( x, y_bottom,
             linestyle = "None",
             color     = colors[0],
             edgecolor = "black", 
             linewidth = 0.5,
             yerr      = yerr_bottom,
             ecolor    = "black",
             capsize   = 2.0,
             label     = bar_labels[0] )

    ## PLOT TOP BARS
    plt.bar( x, y_top,
             bottom    = y_bottom,
             linestyle = "None",
             color     = colors[1],
             edgecolor = "black", 
             linewidth = 0.5,
             yerr      = yerr_top,
             ecolor    = "black",
             capsize   = 2.0,
             label     = bar_labels[1] )
         
    ## PLOT LINE AT Y=0
    plt.plot( [-1, len(xticks)+1 ], 
                [ 0, 0 ],
                linestyle = "-",
                linewidth = 0.5,
                color = "black", )

    ## GET XTICK WIDTH
    x_tick = 1
    ## GET X MAJOR TICKS
    x_ticks = np.arange( 0., len(xticks), 1 )
    ## GET X MINOR TICKS
    x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
    ## SET X TICKS
    ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
    ax.set_xticks( x_ticks, minor = False )       # sets major ticks
    ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
    ax.set_xticklabels( xticks, rotation = 90 ) # sets tick labels        
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )
    
    ## ADD LEGEND
    if len(bar_labels) > 1:
        ax.legend( loc = 'best', ncol = 1, fontsize = 6 )

    fig.set_size_inches( plot_details.width, plot_details.width ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE 2 Y-AXIS BAR PLOT
def plot_2y_line_bar( x, y,
                      yerr       = [ None, None ],
                      title      = None,
                      xlabels    = None,
                      ylabels    = [ r"y label1", r"y label2" ],
                      xticks     = [],
                      yticks     = [ [], [] ],
                      markers    = None,
                      bar_labels = [ None, None ],
                      colors     = None,
                      fig_path   = None, ):
    ## PRINT
    print( "\n--- CREATING BAR PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.15, bottom = 0.25, right = 0.85, top = 0.98 )
    
    ## CREATE TWIN X PLOT
    ax2 = ax.twinx()

    ## SET TITLE
    if title is not None:
        ax.set_title( title )
    
    ## SET Y LABELS
    if ylabels[0] is not None:
        ax.set_ylabel( ylabels[0], labelpad = -4 )
    if ylabels[1] is not None:
        ax2.set_ylabel( ylabels[1], rotation = 270, labelpad = 12, color = "tomato" )

    ## MAKE NONE LIST
    if bar_labels[0] is None:
        bar_labels[0] = len(y[0]) * [ None ]
    if bar_labels[1] is None:
        bar_labels[1] = len(y[1]) * [ None ]

    ## GENERATE ZEROS FOR ERR IF NONE
    if yerr[0] is None:
        yerr[0] = []
        for yy in y[0]:
            yerr[0].append( np.zeros_like(yy) )
    if yerr[1] is None:
        yerr[1] = []
        for yy in y[1]:
            yerr[1].append( np.zeros_like(yy) )

    ## GET MARKERS
    if markers is None:
        markers = len(y[0]) * [ "s" ]

    ## GET COLOR MAP
    if colors is None:
        colors = [ plot_details.colormap(ii) for ii in np.linspace( 0.0, 1.0, len(y[0]+y[1]) ) ]

    ## CREATE SHIFT COUNTER
    jj = 0

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x[1], y[1], yerr[1] )): 
        ## PLOT BARS
        ax2.bar( xx, yy,
                 linestyle = "None",
                 color     = colors[jj],
                 edgecolor = "black", 
                 linewidth = 0.5,
                 yerr      = yyerr,
                 ecolor    = "black",
                 capsize   = 2.0,
                 label     = bar_labels[1][ii] )

        ## UPDATE COUNTER
        jj += 1

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x[0], y[0], yerr[0] )):
        ## PLOT LINES
        ax.plot( xx, yy,
                 linestyle  = "None",
                 marker     = markers[ii],
                 markersize = 6,
                 linewidth  = 1.5,
                 color      = colors[jj],
                 label      = bar_labels[0][ii] )                
        ## PLOT POINTS WITH ERROR BARS
        ax.errorbar( xx, yy,
                     yerr       = yyerr,
                     linestyle  = "None",
                     marker     = markers[ii],
                     markersize = 6.,
                     color      = colors[jj],
                     ecolor     = colors[jj],
                     elinewidth = 0.5, 
                     capsize    = 2., )

        ## UPDATE COUNTER
        jj += 1

    ## PLOT LINE AT Y=0
    ax2.plot( [-1, len(xlabels)+1 ], 
              [ 0, 0 ],
              linestyle = "-",
              linewidth = 0.5,
              color = "black", )
         
    ## SET X AND Y TICKS
    ## GET XTICK WIDTH
    x_tick = 1
    ## GET X MAJOR TICKS
    x_ticks = np.arange( 0., len(xlabels), 1 )
    ## GET X MINOR TICKS
    x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
    ## SET X TICKS
    ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
    ax.set_xticks( x_ticks, minor = False )       # sets major ticks
    ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
    ax.set_xticklabels( xlabels, rotation = 45 ) # sets tick labels
        
    if len(yticks[0]) > 1:
        y_min   = yticks[0][0]
        y_max   = yticks[0][1]
        y_diff  = yticks[0][2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )
    
    if len(yticks[1]) > 1:
        y_min   = yticks[1][0]
        y_max   = yticks[1][1]
        y_diff  = yticks[1][2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax2.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax2.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax2.set_ylim( y_lower, y_upper )
        ax2.tick_params( axis = "y", colors = "tomato" )

    # ## ADD LEGEND
    # if len(y) > 1:
    #     ax.legend( loc = 'best', ncol = 1, fontsize = 6 )

    fig.set_size_inches( plot_details.width, plot_details.width ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE 2 Y-AXIS BAR PLOT
def plot_2y_bar( x, y,
                 yerr       = [ None, None ],
                 title      = None,
                 xlabels    = None,
                 ylabels    = [ r"y label1", r"y label2" ],
                 xticks     = [],
                 yticks     = [ [], [] ],
                 bar_labels = [ None, None ],
                 colors     = None,
                 fig_path   = None, ):
    ## PRINT
    print( "\n--- CREATING BAR PLOT ---")
    
    ## DETERMINE BAR SHIFT
    bar_width = 1 / float(len(y[0]+y[1])+1)
    shift = np.arange( 0, 1. - bar_width, bar_width ) - ( 0.5 - bar_width )

    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.14, bottom = 0.25, right = 0.86, top = 0.98 )
    
    ## CREATE TWIN X PLOT
    ax2 = ax.twinx()

    ## SET TITLE
    if title is not None:
        ax.set_title( title )
    
    ## SET Y LABELS
    if ylabels[0] is not None:
        ax.set_ylabel( ylabels[0] )
    if ylabels[1] is not None:
        ax2.set_ylabel( ylabels[1] )

    ## MAKE NONE LIST
    if bar_labels[0] is None:
        bar_labels[0] = len(y[0]) * [ None ]
    if bar_labels[1] is None:
        bar_labels[1] = len(y[1]) * [ None ]

    ## GENERATE ZEROS FOR ERR IF NONE
    if yerr[0] is None:
        yerr[0] = []
        for yy in y[0]:
            yerr[0].append( np.zeros_like(yy) )
    if yerr[1] is None:
        yerr[1] = []
        for yy in y[1]:
            yerr[1].append( np.zeros_like(yy) )

    ## GET COLOR MAP
    if colors is None:
        colors = [ plot_details.colormap(ii) for ii in np.linspace( 0.0, 1.0, len(y[0]+y[1]) ) ]

    ## CREATE SHIFT COUNTER
    jj = 0

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x[0], y[0], yerr[0] )): 
        ## PLOT BARS
        ax.bar( xx+shift[jj], yy,
                linestyle = "None",
                width     = bar_width,
                color     = colors[jj],
                edgecolor = "black", 
                linewidth = 0.5,
                yerr      = yyerr,
                ecolor    = "black",
                capsize   = 2.0,
                label     = bar_labels[0][ii] )

        ## UPDATE COUNTER
        jj += 1

    ## LOOP THROUGH Y TYPES
    for ii, (xx, yy, yyerr) in enumerate(zip( x[1], y[1], yerr[1] )): 
        ## PLOT BARS
        ax2.bar( xx+shift[jj], yy,
                 linestyle = "None",
                 width     = bar_width,
                 color     = colors[jj],
                 edgecolor = "black", 
                 linewidth = 0.5,
                 yerr      = yyerr,
                 ecolor    = "black",
                 capsize   = 2.0,
                 label     = bar_labels[1][ii] )

        ## UPDATE COUNTER
        jj += 1

    ## PLOT LINE AT Y=0
    ax.plot( [-1, len(xlabels)+1 ], 
             [ 0, 0 ],
             linestyle = "-",
             linewidth = 0.5,
             color = "black", )
         
    ## SET X AND Y TICKS
    ## GET XTICK WIDTH
    x_tick = 1
    ## GET X MAJOR TICKS
    x_ticks = np.arange( 0., len(xlabels), 1 )
    ## GET X MINOR TICKS
    x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
    ## SET X TICKS
    ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
    ax.set_xticks( x_ticks, minor = False )       # sets major ticks
    ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
    ax.set_xticklabels( xlabels, rotation = 45 ) # sets tick labels
        
    if len(yticks[0]) > 1:
        y_min   = yticks[0][0]
        y_max   = yticks[0][1]
        y_diff  = yticks[0][2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )
    
    if len(yticks[1]) > 1:
        y_min   = yticks[1][0]
        y_max   = yticks[1][1]
        y_diff  = yticks[1][2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax2.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax2.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax2.set_ylim( y_lower, y_upper )

    # ## ADD LEGEND
    # if len(y) > 1:
    #     ax.legend( loc = 'best', ncol = 1, fontsize = 6 )

    fig.set_size_inches( plot_details.width, plot_details.width ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## FUNCTION TO CREATE PARITY PLOT
def plot_parity( x, y, 
                 xerr         = None,
                 yerr         = None,
                 fit_line     = None,
                 title        = None,
                 xlabel       = None,
                 ylabel       = None,
                 xticks       = [],
                 yticks       = [],
                 colors       = None,
                 markers      = None,
                 legend       = None,
                 guideline    = 1,
                 legend_cols  = 1,
                 fig_path     = None, ):
    ## PRINT
    print( "\n--- CREATING PARITY PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )

    ## SET X AND Y LABELS
    if xlabel is not None:
        ax.set_xlabel( xlabel )
    if ylabel is not None:
        ax.set_ylabel( ylabel )
    
    ## GENERATE ZEROS FOR ERR IF NONE
    if xerr is None:
        xerr =  np.zeros_like(x)
    if yerr is None:
        yerr = np.zeros_like(y)
    
    ## SET DEFAULT MARKERS
    if markers is None:
        markers = [ "s" ]*len(y)

    ## LOOP THROUGH Y INDICES
    for ii in range(len(y)):
        ## PLOT POINTS WITH ERROR BARS
        ax.errorbar( x[ii], y[ii],
                     xerr       = xerr[ii],
                     yerr       = yerr[ii],
                     linestyle  = "None",
                     marker     = markers[ii],
                     markersize = 6.,
                     color      = colors[ii],
                     ecolor     = colors[ii],
                     elinewidth = 0.5, 
                     capsize    = 2., )
    
    ## ADD LEGEND
    if legend is not None:
        for ll in range(len(legend[0])):
            ## MARKER
            if len(legend) > 2:
                m = legend[2][ll]
            else:
                m = "s"

            ## PLOT WITH SCATTER FOR BETTER LEGEND                
            ax.scatter( [], [],
                        marker = m,
                        s      = 6**2,
                        color  = legend[0][ll],
                        label  = legend[1][ll], )
        ## ADD LEGEND
        ax.legend( loc = 'best', ncol = legend_cols, fontsize = 8 )
        
    ## ADD FITTED LINE
    if fit_line is not None:
        for ii, fit in enumerate(fit_line):
            ## PLOT FIT LINES
            ax.plot( fit[0],
                     fit[1],
                     linestyle = ":",
                     linewidth = 1.5,
                     color = legend[0][ii], )

    ## PLOT X-Y LINE
    if guideline == 1:
        ax.plot( [ xticks[0]-xticks[2], xticks[1]+xticks[2] ], 
                 [ yticks[0]-yticks[2], yticks[1]+yticks[2] ],
                 linestyle = ":",
                 linewidth = 1.5,
                 color = "darkgray" )
    
    if guideline == -1:
        ax.plot( [ xticks[0]-xticks[2], xticks[1]+xticks[2] ], 
                 [ yticks[1]+yticks[2], yticks[0]-yticks[2] ],
                 linestyle = ":",
                 linewidth = 1.5,
                 color = "darkgray" )

    ## SET X AND Y TICKS
    if len(xticks) > 1:
        x_min   = xticks[0]
        x_max   = xticks[1]
        x_diff  = xticks[2]
        x_lower = x_min - 0.5*x_diff
        x_upper = x_max + 0.5*x_diff
        ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
        ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
        ax.set_xlim( x_lower, x_upper )
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        y_upper = y_max + 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_lower, y_upper )
        
    fig.set_size_inches( plot_details.width, plot_details.width ) # 1:1 aspect ratio
    # fig.set_size_inches( 2*plot_details.width, 2*plot_details.width ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE HEAT MAP
def plot_heatmap( data,
                  title    = None,
                  xlabels  = [],
                  ylabels  = [],
                  zrange   = [ -1, 1 ],
                  cmap     = None,
                  fig_path = None, ):
    ## PRINT
    print( "\n--- CREATING HEATMAP ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    plt.rcParams["font.size"] = 6.0
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    # fig.subplots_adjust( left = 0.0, bottom = 0.0, right = 1.0, top = 1.0 )
    fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.98, top = 0.98 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )
    
    ## SET COLOR MAP
    if cmap is None:
        cmap = cm.coolwarm

    ## PLOT HEATMAP
    sns.heatmap( data,
                 xticklabels = xlabels,
                 yticklabels = ylabels,
                 vmin        = -1,
                 vmax        = 1,
                 cmap        = cmap, 
                 ax          = ax,
                 cbar_kws = { "ticks" : np.arange( -1, 1.2, 0.2 ) } )

    fig.set_size_inches( 8.5, 8.5 ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CREATE SURFACE PLOT
def plot_surface( X, Y, Z,
                  title    = None,
                  xlabel   = "xlabel",
                  ylabel   = "ylabel",
                  clabel   = "clabel",
                  xticks   = [],
                  yticks   = [],
                  cticks   = [],
                  cmap     = None,
                  fig_path = None, ):
    ## PRINT
    print( "\n--- CREATING SURFACE PLOT ---")
    
    ## SET PLOT DEFAULT
    plot_details = JACS()
    
    ## CREATE SUBPLOTS
    fig, ax = plt.subplots()
    fig.subplots_adjust( left = 0.15, bottom = 0.15, right = 0.95, top = 0.90 )
    # fig.subplots_adjust( left = 0.15, bottom = 0.25, right = 0.85, top = 0.98 )
    
    ## SET TITLE
    if title is not None:
        ax.set_title( title )

    ## SET X AND Y LABELS
    if xlabel is not None:
        ax.set_xlabel( xlabel )
    if ylabel is not None:
        ax.set_ylabel( ylabel )

    ## SET COLOR MAP
    if cmap is None:
        cmap = cm.coolwarm

    ## SET CBAR BOUNDS
    c_min   = cticks[0]
    c_max   = cticks[1]
    c_diff  = cticks[2]
    c_lower = c_min - 0.5*c_diff
    c_upper = c_max + 0.5*c_diff
    majorticks = np.arange( c_min, c_max + c_diff, c_diff )
    minorticks = np.arange( c_lower, c_max + 1.5*c_diff, c_diff )

    ## PLOT SURFACE MAP
    cs = plt.imshow( Z, cmap = cmap, vmin = c_lower, vmax = c_upper,
                     interpolation = "spline16", origin = "lower",
                     extent = [ xticks[0], xticks[1], yticks[0], yticks[1] ]  )
    cbar = plt.colorbar( cs, ax = ax, aspect = 15 )

    ## ADD TICKS
    cbar.ax.yaxis.set_ticks( majorticks, minor = False )
    cbar.ax.yaxis.set_ticks( minorticks, minor = True )
    cbar.ax.set_ylim( c_lower, c_upper )
    
    ## ADD COLORBAR LABEL
    cbar.ax.set_ylabel( clabel, rotation = 270, labelpad = 12 )

    ## SET X AND Y TICKS
    if len(xticks) > 1:
        x_min   = xticks[0]
        x_max   = xticks[1]
        x_diff  = xticks[2]
        x_lower = x_min - 0.5*x_diff
        ax.set_xticks( np.arange( x_min, x_max + x_diff, x_diff ), minor = False )       # sets major ticks
        ax.set_xticks( np.arange( x_lower, x_max + 1.5*x_diff, x_diff ), minor = True )
        ax.set_xlim( x_min, x_max )
        
    if len(yticks) > 1:
        y_min   = yticks[0]
        y_max   = yticks[1]
        y_diff  = yticks[2]
        y_lower = y_min - 0.5*y_diff
        ax.set_yticks( np.arange( y_min, y_max + y_diff, y_diff ), minor = False )       # sets major ticks
        ax.set_yticks( np.arange( y_lower, y_max + 1.5*y_diff, y_diff ), minor = True )
        ax.set_ylim( y_min, y_max )
                
    fig.set_size_inches( 5.8, 5 ) # 1:1 aspect ratio
    if fig_path is not None:
        print( "FIGURE SAVED TO: %s" % fig_path )
        fig.savefig( fig_path + ".png", dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( fig_path + ".svg", dpi = 300, facecolor = 'w', edgecolor = 'w' )

