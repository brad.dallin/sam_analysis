# -*- coding: utf-8 -*-
"""
plotting_functions.py
script contains plotting functions for SAM simulation data

CREATED ON: 12/14/2020

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS
import os
## IMPORT NUMPY
import numpy as np
## IMPORT MATPLOTLIB
from matplotlib import cm
import matplotlib.pyplot as plt

##############################################################################
## PLOTTING FUNCTIONS
##############################################################################
#def plotting_functions_dict():
#    ## DICTIONARY CONTAIN AVAILABLE ANALYSIS TYPES
#    return {
#             "density"                    : plot_line,
#             "triplet_angle_distribution"         : plot_line,
#             "water_oh_angle_distribution"        : plot_line,
#             "hydration_residence_time"           : plot_line_with_points, 
##             "num_hbonds_all"                     : plot_bar, 
#             "num_hbonds_sam_all"                 : plot_bar,
#             "num_hbonds_water_all"               : plot_bar,
#             "num_hbonds_sam_donor"               : plot_bar,
#             "num_hbonds_water_donor"             : plot_bar,
#             "num_hbonds_sam_acceptor"            : plot_bar,
#             "num_hbonds_water_acceptor"          : plot_bar,
##             "num_hbonds_sam_sam"                 : plot_bar,
##             "num_hbonds_sam_water"               : plot_bar,
#             "num_hbonds_water_sam"               : plot_bar,
##             "num_hbonds_water_water"             : plot_bar,
#             "num_hbonds_sam-water_all_nsam"      : plot_bar,
#             "num_hbonds_sam-water_all_nwater"    : plot_bar,
##             "num_hbonds_sam_water_all"           : plot_bar,
#             "num_hbonds_all"                     : plot_line_with_points,
##             "num_hbonds_sam_all"                 : plot_line_with_points,
##             "num_hbonds_water_all"               : plot_line_with_points,
##             "num_hbonds_sam_donor"               : plot_line_with_points,
##             "num_hbonds_water_donor"             : plot_line_with_points,
##             "num_hbonds_sam_acceptor"            : plot_line_with_points,
##             "num_hbonds_water_acceptor"          : plot_line_with_points,
#             "num_hbonds_sam_sam"                 : plot_line_with_points,
##             "num_hbonds_sam_water"               : plot_line_with_points,
##             "num_hbonds_water-sam"               : plot_line_with_points,
#             "num_hbonds_water_water"             : plot_line_with_points,
##             "num_hbonds_sam-water_all_nsam"      : plot_line_with_points,
##             "num_hbonds_sam-water_all_nwater"    : plot_line_with_points,
#             "num_hbonds_sam_water_all"           : plot_line_with_points,
#             "hbonds_all_distribution"            : plot_line,
#             "hbonds_sam_all_distribution"        : plot_line,
#             "hbonds_water_all_distribution"      : plot_line,
#             "hbonds_sam_donor_distribution"      : plot_line,
#             "hbonds_water_donor_distribution"    : plot_line,
#             "hbonds_sam_acceptor_distribution"   : plot_line,
#             "hbonds_water_acceptor_distribution" : plot_line,
#             "hbonds_sam-sam_distribution"        : plot_line,
#             "hbonds_sam-water_distribution"      : plot_line,
#             "hbonds_water-sam_distribution"      : plot_line,
#             "hbonds_water-water_distribution"    : plot_line,
#             "hbonds_sam-water_all_distribution"  : plot_line,
#             "indus_histograms"                   : plot_histogram,
#             "indus_equil_time"                   : multi_line_plots,
#             "indus_convergence_time"             : multi_line_plots,
#             "indus_pN_distribution"              : plot_line,
#             "indus_hydration_fe"                 : plot_line_with_points,
#             "umbrella_histograms"                : plot_histogram,
#             "umbrella_equilibration_time"        : plot_line,
#             "umbrella_convergence_time"          : plot_line,
#             "umbrella_dewetting_profile"         : plot_line,
#             "umbrella_pmf"                       : plot_line,
#             "umbrella_mf"                        : plot_line,
#             "hyd_force_profile"                  : plot_line,
#             "hyd_interaction_profile"            : plot_line,
#             "hyd_force"                          : plot_bar,
#             "hyd_interaction"                    : plot_bar,
#            }
#
### FUNCTION TO RUN BUILDERS
#def plotting_obj( plotting_type ):
#    """Provided the plotting type, this function provides the plotting function"""
#    plotting_dict = plotting_functions_dict()
#    if plotting_type in plotting_dict.keys():
#        return plotting_dict[plotting_type]
#    else:
#        print( "{} not in available plotting types".format( plotting_type ) )

##############################################################################
## FUNCTIONS & CLASSES
##############################################################################
## LINE PLOT FUNCTION    
def plot_line( ax,
               x_data = [],
               y_data = [],
               x_err  = [],
               y_err  = [],
               colors = [],
               labels = [],):
    """Function to plot lines"""    
    ## ADD ZEROS TO ERROR BARS IF EMPTY
    if len(x_err) < 1:
        x_err = [ np.zeros_like(xx) for xx in x_data ]
        
    if len(y_err) < 1:
        y_err = [ np.zeros_like(yy) for yy in y_data ]
        
    ## ADD COLORS IF EMPTY
    if len(colors) < 1:
        color_iter = np.linspace( 0, 1, len(x_data) )
        colors = [ cm.coolwarm(ii) for ii in color_iter ]
        
    ## ADD NONE TO LABELS IF EMPTY
    if len(labels) < 1:
        labels = [ None ] * len(x_data)
        
    ## LOOP THROUGH DATA TO PLOT
    for ii in range(len(x_data)):
        ## PLOT LINES
        ax.plot( x_data[ii],
                 y_data[ii],
                 linestyle = '-',
                 linewidth = 1.5,
                 color     = colors[ii],
                 label     = labels[ii] )
        
        ## PLOT SHADED ERROR
        ax.plot( x_data[ii],
                 y_data[ii] + y_err[ii],
                 linestyle = '-',
                 linewidth = 1.0,
                 color     = colors[ii] )
        ax.plot( x_data[ii],
                 y_data[ii] - y_err[ii],
                 linestyle = '-',
                 linewidth = 1.0,
                 color     = colors[ii] )
        ax.fill_between( x_data[ii],
                         y_data[ii] + y_err[ii],
                         y_data[ii] - y_err[ii],
                         color = colors[ii],
                         alpha = 0.5, )
    ## RETURN AXIS
    return ax

# CREATE LINE PLOT W/ POINTS
def plot_points( ax,
                           x_data    = [],
                           y_data    = [],
                           x_err     = [],
                           y_err     = [],
                           colors    = [],
                           markers   = [],
                           labels    = [],
                           linestyle = "None" ):
    """Function to plot lines with points"""    
    ## ADD ZEROS TO ERROR BARS IF EMPTY
    if len(x_err) < 1:
        x_err = [ np.zeros_like(xx) for xx in x_data ]
        
    if len(y_err) < 1:
        y_err = [ np.zeros_like(yy) for yy in y_data ]
        
    ## ADD COLORS IF EMPTY
    if len(colors) < 1:
        color_iter = np.linspace( 0, 1, len(x_data) )
        colors = [ cm.coolwarm(ii) for ii in color_iter ]
        
    ## ADD NONE TO LABELS IF EMPTY
    if len(markers) < 1:
        markers = [ None ] * len(x_data)
        
    ## ADD NONE TO LABELS IF EMPTY
    if len(labels) < 1:
        labels = [ None ] * len(x_data)
        
    ## LOOP THROUGH DATA TO PLOT
    for ii in range(len(x_data)):
        ## PLOT LINES
        ax.plot( x_data[ii],
                 y_data[ii],
                 marker     = markers[ii],
                 markersize = 4,
                 linestyle  = linestyle,
                 linewidth  = 1.5,
                 color      = colors[ii],
                 label      = labels[ii] )
            
        ## ADD ERRORBARS TO POINTS
        ax.errorbar( x_data[ii],
                     y_data[ii],
                     marker     = 'None',
                     linestyle  = 'None',
                     xerr       = x_err[ii],
                     yerr       = y_err[ii],
                     ecolor     = colors[ii],
                     elinewidth = 1.5,
                     capsize    = 2.0,
                     capthick   = 1.0, )        

    ## RETURN AXIS
    return ax

### CREATE BAR PLOT
#def plot_bar( data,
#              group_list,
#              out_path,
#              y_ticks,
#              x_label     = r"x label",
#              y_label     = r"y label",
#              group_dict  = None,
#              ncol_legend = None,
#              savefig     = False,
#              **kwargs ):
#    ''''''
#    ## SET PLOT DEFAULT
#    plot_details = JACS()     
#
#    ## CREATING BAR PLOT
#    fig, ax = plt.subplots()
#    fig.subplots_adjust( left = 0.15, bottom = 0.16, right = 0.99, top = 0.97 )
#    ## GIVE PLOT TITLE
#    if "title" in kwargs:
#        plt.title( kwargs["title"], loc = "center" )
#        
#    ## EXTRACT BAR LABELS
##    bar_width = 1 / float(len(group_list)+1)
#    shift = 0 # np.arange( 0, 1. - bar_width, bar_width ) - ( 0.5 - bar_width )
#    ## LOOP THROUGH SAM TYPES CREATING NEW LINE FOR EACH SAM TYPE
#    ii = 0
##    for bar_label in group_list:
#    for bar_label in group_list:
#        ## CHECK IF LABEL IN DESIRED GROUP
#        if bar_label in data.keys():
#            print( bar_label )
#            x = ii + shift
#            y = []
#            for jj, dd in enumerate(data[bar_label]):                 
#                y.append( dd["y"] )
#                    
#            ## CALCULATE AVERAGE
#            y_avg = np.mean( y, axis = 0 )
#            y_err = np.std( y, axis = 0 )
#            
#            ## PLOT BARS
#            plt.bar( x,
#                     y_avg,
#                     linestyle = "None",
#                     color     = "grey",
##                     width     = bar_width,
#                     edgecolor = "black", 
#                     linewidth = 0.5,
#                     yerr      = y_err,
#                     ecolor    = "black",
#                     capsize   = 2.0,
#                     label     = bar_label ) 
#            
#            ## ADD INCREMENT
#            ii += 1
#
#    ## PLOT LINE AT Y=0
#    plt.plot( [ -0.5, len(group_list)-0.5 ], 
#              [ 0, 0 ],
#              linestyle = "-",
#              linewidth = 0.5,
#              color = "black",
#              )
#
#    ## SET LEGEND
##    ax.legend( loc = 'center left', ncol = ncol_legend, bbox_to_anchor = ( 1.0, 0.5 ) )
##    ax.legend( loc = 'best', ncol = ncol_legend )
#         
#    ## GET XTICK WIDTH
#    x_tick = 1
#    ## GET X MAJOR TICKS
#    x_ticks = np.arange( 0., len(group_list), 1 )
#    ## GET X MINOR TICKS
#    x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
#    ## SET X TICKS
#    ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
#    ax.set_xticks( x_ticks, minor = False )       # sets major ticks
#    ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
#    ax.set_xticklabels( group_list, rotation = 45 ) # sets tick labels
#    
#    ## GET YTICK WIDTH
#    y_tick = y_ticks[1] - y_ticks[0]
#    ## GET Y MINOR TICKS
#    y_minor_ticks = np.concatenate(( y_ticks - 0.5*y_tick, np.array([ y_ticks[-1] + 0.5*y_tick ]) ))
#    ## SET Y TICKS
#    ax.set_ylim( y_minor_ticks[0], y_minor_ticks[-1] )
#    ax.set_yticks( y_ticks, minor = False )       # sets major ticks
#    ax.set_yticks( y_minor_ticks, minor = True )  # sets minor ticks
#    
#    # SET Y AXES
#    ax.set_ylabel( y_label )
#    
#    fig.set_size_inches( plot_details.width, plot_details.height )
#    fig.tight_layout()
#    if savefig is True:
#        print( "FIGURE SAVED TO: %s" % out_path )
#        fig.savefig( out_path, dpi = 300, facecolor = 'w', edgecolor = 'w' )
#    
#        
### CREATE LINE PLOT
#def multi_line_plots( data,
#                      group_list,
#                      out_path,
#                      x_ticks     = [],
#                      y_ticks     = [],
#                      x_label     = r"x label",
#                      y_label     = r"y label",
#                      ncol_legend = None,
#                      savefig     = False,
#                      **kwargs ):
#    r"""
#    """
#    ## SET PLOT DEFAULT
#    plot_details = JACS()
#    
#    ## LOOP THROUGH LINE PLOTS
#    for plot_label in group_list:
#        ## CHECK IF LABEL IN DESIRED GROUP
#        if plot_label in data.keys():
#            ## CREATING LINE PLOT
#            fig, ax = plt.subplots()
#            fig.subplots_adjust( left = 0.15, bottom = 0.16, right = 0.99, top = 0.97 )
#            
#            ## GIVE PLOT TITLE
#            plt.title( plot_label, loc = "center" )
#            
#            ## COMPILE X AND Y DATA (GET ERROR BARS)
#            x, y = [], []
#            for dd in data[plot_label]:
#                x.append( dd["x"] ) 
#                y.append( dd["y"] )
#            x_avg = np.mean( x, axis = 0 )
#            x_err = np.std( x, axis = 0 )
#            y_avg = np.mean( y, axis = 0 )
#            y_err = np.std( y, axis = 0 )
#            ## REMOVE INF
#            x_avg = x_avg[np.isfinite( y_avg )]
#            x_err = x_err[np.isfinite( y_avg )]
#            y_err = y_err[np.isfinite( y_avg )]
#            y_avg = y_avg[np.isfinite( y_avg )]
#            
#            ## PLOT LINES
#            plt.plot( x_avg,
#                      y_avg,
#                      linestyle = '-',
#                      linewidth = 1.5,
#                      color     = "black" )
#            
#            ## PLOT SHADED ERROR
#            plt.plot( x_avg,
#                      y_avg + y_err,
#                      linestyle = '-',
#                      linewidth = 1.0,
#                      color     = "black" )
#            plt.plot( x_avg,
#                      y_avg - y_err,
#                      linestyle = '-',
#                      linewidth = 1.0,
#                      color     = "black" )
#            plt.fill_between( x_avg,
#                              y_avg + y_err,
#                              y_avg - y_err,
#                              color = "black",
#                              alpha = 0.5, )
#                
#            # SET X AXES
#            ax.set_xlabel( x_label )
#            ## GET XTICK WIDTH
#            x_tick = 500
#            xmin = 0 # np.floor( x_avg.min() ).astype('int')
#            xmax = 2500 # np.floor( x_avg.max() ).astype('int') + x_tick
#            ## GET X MAJOR TICKS
#            x_ticks = np.arange( xmin, xmax, x_tick )
#            ## GET X MINOR TICKS
#            x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
#            ## SET X TICKS
#            ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
#            ax.set_xticks( x_ticks, minor = False )       # sets major ticks
#            ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
#            # SET Y AXES
#            ax.set_ylabel( y_label )
#            ## GET YTICK WIDTH
#            y_tick = 1
#            ymin = np.floor( y_avg.min() ).astype('int') - 2*y_tick
#            ymax = np.floor( y_avg.max() ).astype('int') + 2*y_tick
#            ## GET X MAJOR TICKS
#            y_ticks = np.arange( ymin, ymax, y_tick )
#            ## GET Y MINOR TICKS
#            y_minor_ticks = np.concatenate(( y_ticks - 0.5*y_tick, np.array([ y_ticks[-1] + 0.5*y_tick ]) ))
#            ## SET Y TICKS
#            ax.set_ylim( y_minor_ticks[0], y_minor_ticks[-1] )
#            ax.set_yticks( y_ticks, minor = False )       # sets major ticks
#            ax.set_yticks( y_minor_ticks, minor = True )  # sets minor ticks
#            
#            fig.set_size_inches( plot_details.width, plot_details.height ) # *1.4
#            fig.tight_layout()
#            if savefig is True:
#                ext = os.path.splitext( out_path )[-1]
#                path_fig = out_path.strip(ext) + "_" + plot_label.lower() + ext
#                print( "FIGURE SAVED TO: %s" % path_fig )
#                fig.savefig( path_fig, dpi = 300, facecolor = 'w', edgecolor = 'w' )
#                ## CLOSE FIGURES
#                plt.close()
#
### CREATE HISTOGRAM PLOT
#def plot_histogram( data,
#                    group_list,
#                    out_path,
#                    x_ticks,
#                    y_ticks,
#                    x_label     = r"x label",
#                    y_label     = r"y label",
#                    ncol_legend = None,
#                    savefig     = False,
#                    **kwargs ):
#    r"""
#    """
#    ## SET PLOT DEFAULT
#    plot_details = JACS()
#    
#    ## LOOP THROUGH SAM TYPES CREATING NEW PLOT FOR EACH SAM TYPE
#    for plot_label, plot_data in data.items():
#        ## CHECK IF LABEL IN DESIRED GROUP
#        if plot_label in group_list:
#            ## GIVE PLOT UNIQUE NAME
#            add_string = plot_label.lower()
#            split_path = out_path.split(".")
#            new_path = split_path[0] + "_" + add_string + "." + split_path[1]
#            ## CREATING LINE PLOT
#            fig, ax = plt.subplots()
#            fig.subplots_adjust( left = 0.15, bottom = 0.16, right = 0.99, top = 0.97 )
#        
#            ## GIVE PLOT A TITLE
#            plt.title( plot_label, loc = "center" )
#            
#            ## COMPILE X AND Y DATA (CAN BE USED TO GET ERROR BARS)
#            x, y = [], []
#            for dd in plot_data:
#                x.append( dd["x"] ) 
#                y.append( dd["y"] )
#            x_avg = np.sum( x, axis = 0 )
#            x_err = np.std( x, axis = 0 )
#            y_avg = np.sum( y, axis = 0 )
#            y_err = np.std( y, axis = 0 )
#                
#            ## PLOT LINES
#            plt.plot( x_avg,
#                      y_avg,
#                      linestyle = '-',
#                      linewidth = 1.5,
#                      color     = "black" )
#           
#            # SET X AXES
#            ax.set_xlabel( x_label )
#            ## GET YTICK WIDTH
#            x_tick = x_ticks[1] - x_ticks[0]
#            ## GET X MINOR TICKS
#            x_minor_ticks = np.concatenate(( x_ticks - 0.5*x_tick, np.array([ x_ticks[-1] + 0.5*x_tick ]) ))
#            ## SET X TICKS
#            ax.set_xlim( x_minor_ticks[0], x_minor_ticks[-1] )
#            ax.set_xticks( x_ticks, minor = False )       # sets major ticks
#            ax.set_xticks( x_minor_ticks, minor = True )  # sets minor ticks
#            # SET Y AXES
#            ax.set_ylabel( y_label )
#            ## GET YTICK WIDTH
#            y_tick = y_ticks[1] - y_ticks[0]
#            ## GET Y MINOR TICKS
#            y_minor_ticks = np.concatenate(( y_ticks - 0.5*y_tick, np.array([ y_ticks[-1] + 0.5*y_tick ]) ))
#            ## SET Y TICKS
#            ax.set_ylim( y_minor_ticks[0], y_minor_ticks[-1] )
#            ax.set_yticks( y_ticks, minor = False )       # sets major ticks
#            ax.set_yticks( y_minor_ticks, minor = True )  # sets minor ticks
#            
#            fig.set_size_inches( plot_details.width, plot_details.height )
#            fig.tight_layout()
#            if savefig is True:
#                print( "FIGURE SAVED TO: %s" % new_path )
#                fig.savefig( new_path, dpi = 300, facecolor = 'w', edgecolor = 'w' )
#                ## CLOSE FIGURES
#                plt.close()
#
#GROUP_DICT = { 
#                ## NH2 CHARGE SCALED
#                "NH2-SCALED" :
#                [ "NH2-K0.00", "NH2-K0.10", "NH2-K0.20", "NH2-K0.30", "NH2-K0.40", "NH2-K0.50",
#                  "NH2-K0.60", "NH2-K0.70", "NH2-K0.80", "NH2-K0.90", "NH2-SINGLE", ],
#                ## CONH2 CHARGE SCALED
#                "CONH2-SCALED" :
#                [ "CONH2-K0.00", "CONH2-K0.10", "CONH2-K0.20", "CONH2-K0.30", "CONH2-K0.40", "CONH2-K0.50",
#                  "CONH2-K0.60", "CONH2-K0.70", "CONH2-K0.80", "CONH2-K0.90", "CONH2-SINGLE", ],
#                ## OH CHARGE SCALED
#                "OH-SCALED" :
#                [ "OH-K0.00", "OH-K0.10", "OH-K0.20", "OH-K0.30", "OH-K0.40", "OH-K0.50",
#                  "OH-K0.60", "OH-K0.70", "OH-K0.80", "OH-K0.90", "OH-SINGLE", ],
#                ## NH2 MIXED COMPOSITION
#                "NH2-MIXED" :
#                [ "CH3-SINGLE", "CH3-NH2-M0.25", "CH3-NH2-M0.40",
#                  "CH3-NH2-M0.50", "CH3-NH2-M0.75", "NH2-SINGLE", ],
#                ## CONH2 MIXED COMPOSITION
#                "CONH2-MIXED" :
#                [ "CH3-SINGLE", "CH3-CONH2-M0.25", "CH3-CONH2-M0.40",
#                  "CH3-CONH2-M0.50", "CH3-CONH2-M0.75", "CONH2-SINGLE", ],
#                ## OH MIXED COMPOSITION
#                "OH-MIXED" :
#                [ "CH3-SINGLE", "CH3-OH-M0.25", "CH3-OH-M0.40",
#                  "CH3-OH-M0.50", "CH3-OH-M0.75", "OH-SINGLE", ],
#                ## NH2 SEPARATED COMPOSITION
#                "NH2-SEPARATED" :
#                [ "CH3-SINGLE", "CH3-NH2-S0.25", "CH3-NH2-S0.42",
#                  "CH3-NH2-S0.50", "CH3-NH2-S0.75", "NH2-SINGLE", ],
#                ## CONH2 SEPARATED COMPOSITION
#                "CONH2-SEPARATED" :
#                [ "CH3-SINGLE", "CH3-CONH2-S0.25", "CH3-CONH2-S0.42",
#                  "CH3-CONH2-S0.50", "CH3-CONH2-S0.75", "CONH2-SINGLE", ],
#                ## OH SEPARATED COMPOSITION
#                "OH-SEPARATED" :
#                [ "CH3-SINGLE", "CH3-OH-S0.25", "CH3-OH-S0.42",
#                  "CH3-OH-S0.50", "CH3-OH-S0.75", "OH-SINGLE", ],
#                ## BULK
#                "BULK" :
#                [ "BULK-TIP4P-ICE", "BULK-TIP4P", "BULK-LENNARD-JONES", "BULK-IDEAL-GAS", ],
#                }
#        
##%%
###############################################################################
### TESTING SCRIPT
###############################################################################
#if __name__ == "__main__":
#    ## IMPORT CHECK SERVER PATH
#    from sam_analysis.core.check_tools import check_server_path
#    
#    ## TESTING DIRECTORY
#    test_dir = r"/mnt/r/python_projects/sam_analysis/sam_analysis/testing"
#    
#    ## SAM DIRECTORY
#    sam_dir = r"mixed_conh2_sam"
#    
#    ## WORKING DIR
#    working_dir = os.path.join( test_dir, sam_dir )
#        
#    ## DATA FILE NAME
#    data_pkl = "unbiased_results_data.pkl"
#    
#    ## CHECK PATH TO DATA
#    path_data = os.path.join( working_dir, "output_files", data_pkl )
#    path_data = check_server_path( path_data )
#    
#    ## LOAD DATA
#    data = load_pkl( path_data )
#    
#    ## DENSITY
#    plot_key = "density"
#    
