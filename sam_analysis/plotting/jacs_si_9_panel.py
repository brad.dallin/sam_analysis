"""
plot_styles.py
This script contains the class which constructs the plotting tool object
when adding an new tool must add a builder object for that tool. Follow the example
of one of the classed under plotting classes

CREATED ON: 12/14/2020

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS
import os
## IMPORT NUMPY
import numpy as np
## IMPORT MATPLOTLIB PYPLOT
from matplotlib import cm
import matplotlib.pyplot as plt

## FUNCTION TO SAVE AND LOAD PICKLE FILES
from sam_analysis.core.pickles import load_pkl
## IMPORT LINE PLOT FUNCTION
from sam_analysis.plotting.plot_types import plot_line
## IMPORT PLOT STYLES
from sam_analysis.plotting.plot_styles import plot_style

##############################################################################
## FUNCTIONS & CLASSES
##############################################################################
## JACS SI CLASS
class JacsSiNinePanel:
    """Class object to plot SI figures for JACS jornals"""
    
    def __init__( self ):
        """Initialize by resetting plotting parameters"""   
        ## SET FONT PARAMETERS
        plt.rcParams["font.sans-serif"]  = "Arial"
        plt.rcParams["font.family"]      = "sans-serif"
        plt.rcParams["font.size"]        = 8.0
        
        ## SET GRID PARAMETERS
        plt.rcParams["axes.grid"]        = True
        plt.rcParams["grid.linestyle"]   = ":"
        plt.rcParams["grid.linewidth"]   = "0.5"
        plt.rcParams["grid.color"]       = "grey"
        
        ## SET LEGEND PARAMETERS
        plt.rcParams["legend.loc"]       = "best"
        plt.rcParams["legend.fancybox"]  = True
        plt.rcParams["legend.facecolor"] = "white"
        plt.rcParams["legend.edgecolor"] = "black"
        
        ## SET FIGURE DIMENSIONS
        self.width  = 7.
        self.height = 0.75 * self.width

    ## INSTANCE PREPARES SUBPLOTS
    def prepare_subplots( self,
                          plot_type,
                          rows = [ r"NH$_{2}$", r"CONH$_{2}$", r"OH" ],
                          cols = [ "Charge Scaled", "Mixed", "Separated" ] ):
        """Create subplots"""
        ## GET NUM ROWS
        if type(rows) is list:
            n_rows = len(rows)
        else:
            n_rows = rows
        
        ## GET NUM COLS
        if type(cols) is list:
            n_cols = len(cols)
        else:
            n_cols = cols        
        
        ## INITIALIZE SUBPLOTS
        fig, axs = plt.subplots( n_rows, n_cols )
        fig.subplots_adjust( left = 0.12, bottom = 0.08, right = 0.98, top = 0.95 )
        fig.set_size_inches( self.width, self.height )
        
        ## LOOP THROUGH ROWS
        for ii in range(n_rows):
            ## LOOP THROUGH COLUMNS
            for jj in range(n_cols):
                ## ADD STYLE TO PLOT
                if jj == 0 and ii == n_rows-1:
                    axs[ii,jj] = plot_style( axs[ii,jj], plot_type, add_xlabel = True, add_ylabel = True )                    
                elif jj == 0:
                    axs[ii,jj] = plot_style( axs[ii,jj], plot_type, add_xlabel = False, add_ylabel = True )  
                elif ii == n_rows-1:
                    axs[ii,jj] = plot_style( axs[ii,jj], plot_type, add_xlabel = True, add_ylabel = False )
                else:
                    axs[ii,jj] = plot_style( axs[ii,jj], plot_type, add_xlabel = False, add_ylabel = False )
                    
        ## ADD ROW LABELS TO SUBPLOTS (IF LIST ROW INPUTS)
        if type(rows) is list:
            for ii, rl in enumerate( rows ):
                ## GET SUBPLOT POSITION
                box = axs[ii,0].get_position()
                
                ## TEXT FIGURE POSITION
                x = box.x0 - 0.09
                y = box.y1 - 0.5 * box.height
                
                ## ADD LABEL
                props = { "ha": "center", "va": "center" }
                plt.gcf().text( x, y, rl, props, fontsize = 11, rotation = 90 )                    

        ## ADD COLUMN LABELS TO SUBPLOTS (IF LIST COLUMN INPUTS)
        if type(cols) is list:
            for jj, cl in enumerate( cols ):
                ## GET SUBPLOT POSITION
                box = axs[0,jj].get_position()
                
                ## TEXT FIGURE POSITION
                x = box.x0 + 0.5 * box.width
                y = box.y1 + 0.03
                
                ## ADD LABEL
                props = { "ha": "center", "va": "center" }
                plt.gcf().text( x, y, cl, props, fontsize = 10 ) 
        
        return fig, axs
                    
    ## INSTANCE ADDS DATA TO SUBPLOTS
    def add_data( self, axs, plot_data ):
        """Function plotting data on subplots"""
        ## GET DATA SIZE
        n_rows = len(plot_data)
        n_cols = len(plot_data[0])
        
        ## PLOT DATA
        for ii in range(n_rows):
            for jj in range(n_cols):
                ## EXTRACT DATA
                x_data = plot_data[ii][jj]["x"]
                y_data = plot_data[ii][jj]["y"]
                x_err  = plot_data[ii][jj]["xerr"]
                y_err  = plot_data[ii][jj]["yerr"]
                colors = plot_data[ii][jj]["colors"]
                labels = plot_data[ii][jj]["labels"]
                
                ## PLOT DATA
                axs[ii,jj] = plot_line( ax     = axs[ii,jj], 
                                        x_data = x_data,
                                        y_data = y_data,
                                        x_err  = x_err,
                                        y_err  = y_err,
                                        colors = colors,
                                        labels = labels )
                ## ADD LEGEND
                if ii == 0:
                    axs[ii,jj].legend( loc           = 'upper right',
                                       ncol          = 2,
                                       fontsize      = 6,
                                       labelspacing  = 0.2,
                                       columnspacing = 1.0,
                                       borderpad     = 0.4,
                                       handlelength  = 1.0, )
        ## RETURN AXES
        return axs
    
    ## INSTANCE TO SAVE PLOTS
    def save_plot( self, fig, path_fig ):        
        ## SAVE FIG AS PNG AND SVG
        print( "FIGURE SAVED TO: %s" % path_fig )
        png = path_fig + ".png"
        svg = path_fig + ".svg"
        fig.savefig( png, dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( svg, dpi = 300, facecolor = 'w', edgecolor = 'w' )

## 9 PANEL FIGURE
NINE_PANEL_LABELS = [
                      ## CHARGE SCALED NH2
                      [ "CS0.0NH2", "CS0.1NH2", "CS0.2NH2", "CS0.3NH2", "CS0.4NH2", "CS0.5NH2",
                        "CS0.6NH2", "CS0.7NH2", "CS0.8NH2", "CS0.9NH2", "NH2" ],
                      ## CHARGE SCALED CONH2
                      [ "CS0.0CONH2", "CS0.1CONH2", "CS0.2CONH2", "CS0.3CONH2", "CS0.4CONH2", "CS0.5CONH2",
                        "CS0.6CONH2", "CS0.7CONH2", "CS0.8CONH2", "CS0.9CONH2", "CONH2" ],
                      ## CHARGE SCALED OH
                      [ "CS0.0OH", "CS0.1OH", "CS0.2OH", "CS0.3OH", "CS0.4OH", "CS0.5OH",
                        "CS0.6OH", "CS0.7OH", "CS0.8OH", "CS0.9OH", "OH" ],
                      ## MIXED NH2
                      [ "CH3", "MIX25NH2", "MIX40NH2", "MIX50NH2", "MIX75NH2", "NH2" ],
                      ## MIXED CONH2
                      [ "CH3", "MIX25CONH2", "MIX40CONH2", "MIX50CONH2", "MIX75CONH2", "CONH2" ],
                      ## MIXED OH
                      [ "CH3", "MIX25OH", "MIX40OH", "MIX50OH", "MIX75OH", "OH" ],
                      ## SEPARATED NH2
                      [ "CH3", "SEP25NH2", "SEP40NH2", "SEP50NH2", "SEP75NH2", "NH2" ],
                      ## SEPARATED CONH2
                      [ "CH3", "SEP25CONH2", "SEP40CONH2", "SEP50CONH2", "SEP75CONH2", "CONH2" ],
                      ## SEPARATED OH
                      [ "CH3", "SEP25OH", "SEP40OH", "SEP50OH", "SEP75OH", "OH" ],
                      ]

## 9 PANEL LEGENDS
NINE_PANEL_LEGENDS = [
                       ## CHARGE SCALED
                       [ r"$k=$" + "{:.2f}".format( ii ) for ii in np.arange( 0, 1.1, 0.1 ) ],
                       [ r"$k=$" + "{:.2f}".format( ii ) for ii in np.arange( 0, 1.1, 0.1 ) ],
                       [ r"$k=$" + "{:.2f}".format( ii ) for ii in np.arange( 0, 1.1, 0.1 ) ],
                       ## MIXED/SEPARATED
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       [ r"$\sigma=$" + "{:.2f}".format( ii ) for ii in [ 0.0, 0.25, 0.40, 0.50, 0.75, 1.0 ] ],
                       ]

## PLOT TYPE LABELS
PLOT_TYPE_LABELS = { "density"             : [ "density_z", "water_density" ],
                     "triplet_angles"      : [ "triplet_angle_theta", "water_water_water_distribution" ],
                     "oh_bond_orientation" : [ "orientation_phi", "oh_bond_angle_distribution" ],
                     "hbonds_total"        : [ "hbonds_dist_n", "hbonds_dist_total" ],
                     "hbonds_sam_sam"      : [ "hbonds_dist_n", "hbonds_dist_sam_sam" ],
                     "hbonds_sam_water"    : [ "hbonds_dist_n", "hbonds_dist_sam_water_per_water" ],
                     "hbonds_water_water"  : [ "hbonds_dist_n", "hbonds_dist_water_water" ],
                     }
        
#%%
##############################################################################
## TESTING SCRIPT
##############################################################################
if __name__ == "__main__":
    ## IMPORT CHECK SERVER PATH
    from sam_analysis.core.check_tools import check_server_path
    
    ## PLOT TYPE
    plot_type = "hbonds_total"
    
    ## X & Y LABELS
    xl = PLOT_TYPE_LABELS[plot_type][0]
    yl = PLOT_TYPE_LABELS[plot_type][1]
    
    ## DATA DIRECTORY
    data_dir = r"/mnt/r/python_projects/sam_analysis/sam_analysis/raw_data"
    
    ## SAM DIRECTORY
    data_pkl = r"unbiased_plotting_data.pkl"
    
    ## PATH FIG
    path_fig = r"water_{}_9_panel".format( plot_type )
    
    ## CHECK PATH TO DATA
    path_data = os.path.join( data_dir, data_pkl )
    path_data = check_server_path( path_data )
    
    ## PATH TO FIGURE
    path_fig = os.path.join( data_dir, path_fig )
    path_fig = check_server_path( path_fig )
    
    ## LOAD DATA
    data = load_pkl( path_data )
    
    ## RESTRUCTURE FOR PLOTTING
    plot_data = []
    ## LOOP THROUGH PANELS
    for panel_labels, legend_labels in zip(NINE_PANEL_LABELS, NINE_PANEL_LEGENDS):
        ## PLACE HOLDER FOR PANEL DATA
        panel_data = { "x"      : [],
                       "y"      : [],
                       "xerr"   : [],
                       "yerr"   : [],
                       "colors" : [],
                       "labels" : [], }
        
        ## COLORS
        colors = [ cm.coolwarm(ii) for ii in np.linspace( 0, 1, len(panel_labels) ) ]
        
        ## LOOP THROUGH LABELS
        for pl, ll, cc in zip(panel_labels, legend_labels, colors):
            ## EXTRACT DATA OF INTEREST
            x_data = data[pl][xl].mean( axis = 0 )
            x_err  = data[pl][xl].std( axis = 0 )
            d = data[pl][yl]
            y_data = d.mean( axis = 0 )
            y_err  = d.std( axis = 0 )
            panel_data["x"].append( x_data )
            panel_data["y"].append( y_data )
            panel_data["xerr"].append( x_err )
            panel_data["yerr"].append( y_err )
            panel_data["colors"].append( cc )
            panel_data["labels"].append( ll )
        
        ## UPDATE PLOT DATA
        plot_data.append( panel_data )
    
    ## MAKE PLOT DATA 2D
    plot_data = [ [ plot_data[3*ii+jj] for ii in range(3) ] for jj in range(3) ]
    
    ## INITIALIZE PLOTS
    plot_obj = JacsSiNinePanel()
    
    ## CREATE SUBPLOTS
    fig, axs = plot_obj.prepare_subplots( plot_type,
                                          rows = [ r"NH$_{2}$", r"CONH$_{2}$", r"OH" ], 
                                          cols = [ "Charge Scaled", "Mixed", "Separated" ] )
    
    ## PLOT DATA
    axs = plot_obj.add_data( axs, plot_data )
    
    ## SAVE FIG
    plot_obj.save_plot( fig, path_fig )