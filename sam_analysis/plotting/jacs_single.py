"""
plot_styles.py
This script contains the class which constructs the plotting tool object
when adding an new tool must add a builder object for that tool. Follow the example
of one of the classed under plotting classes

CREATED ON: 12/14/2020

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT MATPLOTLIB PYPLOT
from matplotlib import cm
import matplotlib.pyplot as plt

## IMPORT NUMPY
import numpy as np

##############################################################################
## FUNCTIONS & CLASSES
##############################################################################
class JACS:
    """class adusts matplotlib defaults to higher quality images
     JACS single format: single column 3.33 in. wide
                         double column 4.167-7 in. wide
                         all max 9.167 in. height (including caption)  
                         font >=4.5 pt
                         linewidth >=0.5 pt
                         font style Helvetica or Arial"""
    def __init__( self ):
        """Initialize by resetting plotting parameters"""   
        ## SET FONT PARAMETERS
        plt.rcParams["font.sans-serif"]  = "DejaVu Sans"
        plt.rcParams["font.family"]      = "sans-serif"
        plt.rcParams["font.size"]        = 8.0
        
        ## SET GRID PARAMETERS
        plt.rcParams["axes.grid"]        = False
        plt.rcParams["grid.linestyle"]   = ":"
        plt.rcParams["grid.linewidth"]   = "0.5"
        plt.rcParams["grid.color"]       = "grey"
        
        ## SET LEGEND PARAMETERS
        plt.rcParams["legend.loc"]       = "best"
        plt.rcParams["legend.fancybox"]  = True
        plt.rcParams["legend.facecolor"] = "white"
        plt.rcParams["legend.edgecolor"] = "black"
        
        ## LIST OF MARKER STYLES
        self.markers = { 
                        "SCALED"    : "s",
                        "MIXED"     : "^",
                        "SEPARATED" : "o",
                        "BULK"      : "v",
                        }
        self.marker_labels = [ "SCALED", "MIXED", "SEPARATED", "BULK" ]
        self.n_markers = len(self.markers)
        
        ## LIST OF COLORS
        self.colors = { 
                        "CH3"   : "dimgrey",
                        "NH2"   : "slateblue",
                        "CONH2" : "darkseagreen",
                        "OH"    : "tomato",
                        "BULK"  : "black",
                        }
        self.color_labels = [ "CH3", "NH2", "CONH2", "OH", "BULK" ]
        self.n_colors = len(self.colors)
        
        ## SET DEFAULT COLORMAP (coolwarm is somewhat color vision deciency friendly)
        self.colormap = cm.coolwarm
        
        self.width  = 3.33 # in
        self.height = 0.75 * self.width # 4:3 aspect rati       
 
## DEFAULTS FOR UMBRELLA SAMPLING
class UnbiasedDefaults:
    """Class containing default plotting variables for umbrella sampling"""
    def __init__( self, plot_type ):
        ## IF PLOT TYPE IS HISTOGRAM
        if plot_type == "density":
            self.add_density_defaults()
        elif plot_type == "triplet_angle_distribution":
            self.add_triplet_distribution_defaults()
        elif plot_type == "water_oh_angle_distribution":
            self.add_oh_angle_distribution_defaults()
        elif plot_type == "hydration_residence_time":
            self.add_hydration_residence_defaults()
        elif "num_hbonds" in plot_type:
            self.add_hbonds_defaults()
        elif "hbonds_distribution" in plot_type:
            self.add_hbonds_defaults()
        elif "wc_height_difference_map" in plot_type:
            self.add_wc_height_map_defaults()
        elif "wc_height_variance_map" in plot_type:
            self.add_wc_variance_map_defaults()
        elif "voronoi_diagram" in plot_type:
            self.add_voronoi_defaults()
            
    ## DENSITY PROFILE DEFAULTS
    def add_density_defaults( self ):
        """Method adding density plotting defaults"""
        ## ADD X LABEL
        self.xlabel = "Distance from interface (nm)"
        ## ADD Y LABEL
        self.ylabel = r"$\rho$"
        ## ADD X TICKS
        self.xticks = np.arange( -2., 3., 0.5 )
        ## ADD Y TICKS
        self.yticks = np.arange( 0.0, 1.8, 0.2 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 2
        
    ## TRIPLET ANGLE DISTRIBUTION DEFAULTS
    def add_triplet_distribution_defaults( self ):
        """Method adding triplet angle distribution defaults"""
        ## ADD X LABEL
        self.xlabel = r"$\theta$ (degrees)"
        ## ADD Y LABEL
#        self.ylabel = r"$\it{p}(\theta)-\it{p}_{CH_{3}}(\theta)$" #  x 10^{4}
        self.ylabel = r"$\it{p}(\theta)$"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 200.0, 20.0 )
        ## ADD Y TICKS
#        self.yticks = np.arange( -10.0, 15.0, 5.0 ) * 10e-5
#        self.yticks = np.arange( -60.0, 140.0, 20.0 ) * 10e-5
        self.yticks = np.arange( 0.0, 0.0325, 0.005 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## OH ANGLE DISTRIBUTION DEFAULTS
    def add_oh_angle_distribution_defaults( self ):
        """Method adding water oh angle distribution defaults"""
        ## ADD X LABEL
        self.xlabel = r"$\phi$ (degrees)"
        ## ADD Y LABEL
        self.ylabel = r"$\it{p}(\phi)$"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 200.0, 20.0 )
        ## ADD Y TICKS
        self.yticks = np.arange( 0.0, 0.02, 0.005 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## HYDRATION RESIDENCE TIME DEFAULTS
    def add_hydration_residence_defaults( self ):
        """Method adding hydration residence time defaults"""
        ## ADD X LABEL
        self.xlabel = "Fraction"
        ## ADD Y LABEL
        self.ylabel = "Hydration residence time (ps)"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 1.2, 0.2 )
        ## ADD Y TICKS
        self.yticks = np.arange( 50.0, 80.0, 5.0 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## HBOND DEFAULTS
    def add_hbonds_defaults( self ):
        """Method adding hbonds defaults"""
        ## ADD X LABEL
        self.xlabel = "Fraction"
        ## ADD Y LABEL
        self.ylabel = "Num. of Hydrogen Bonds"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 1.2, 0.2 )
        ## ADD Y TICKS
        self.yticks = np.arange( 0.0, 2.4, 0.4 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 2
    
    ## WC HEIGHT DEFAULTS
    def add_wc_height_map_defaults( self ):
        """Method adding wc height map defaults"""
        ## ADD X LABEL
        self.xlabel = "x (nm)"
        ## ADD Y LABEL
        self.ylabel = "y (nm)"
        ## ADD CBAR LABEL
        self.clabel = r"Height ($\AA$)"
        ## GET X TICK WIDTH
        x_tick = 1
        ## GET X MAJOR TICKS
        self.major_xticks = np.arange( 0, 6, x_tick )
        ## GET X MINOR TICKS
        self.minor_xticks = np.concatenate(( self.major_xticks - 0.5*x_tick, 
                                np.array([ self.major_xticks[-1] + 0.5*x_tick ]) ))
        ## GET Y TICK WIDTH
        y_tick = 1
        ## GET Y MAJOR TICKS
        self.major_yticks = np.arange( 0, 7, y_tick )
        ## GET Y MINOR TICKS
        self.minor_yticks = np.concatenate(( self.major_yticks - 0.5*y_tick, 
                                np.array([ self.major_yticks[-1] + 0.5*y_tick ]) ))
        ## GET CBAR TICK WIDTH
        c_tick = 0.2
        ## GET CBAR MAJOR TICKS
        self.major_cticks = np.arange( -0.4, 0.6, c_tick )
        ## GET CBAR MINOR TICKS
        self.minor_cticks = np.concatenate(( self.major_cticks - 0.5*c_tick, 
                                np.array([ self.major_cticks[-1] + 0.5*c_tick ]) ))
    
    ## WC HEIGHT DEFAULTS
    def add_wc_variance_map_defaults( self ):
        """Method adding wc height variance map defaults"""
        ## ADD X LABEL
        self.xlabel = "x (nm)"
        ## ADD Y LABEL
        self.ylabel = "y (nm)"
        ## ADD CBAR LABEL
        self.clabel = "Height variance x $10^{-2}$ ($\AA^{2}$)"
        ## GET X TICK WIDTH
        x_tick = 1
        ## GET X MAJOR TICKS
        self.major_xticks = np.arange( 0, 6, x_tick )
        ## GET X MINOR TICKS
        self.minor_xticks = np.concatenate(( self.major_xticks - 0.5*x_tick, 
                                np.array([ self.major_xticks[-1] + 0.5*x_tick ]) ))
        ## GET Y TICK WIDTH
        y_tick = 1
        ## GET Y MAJOR TICKS
        self.major_yticks = np.arange( 0, 7, y_tick )
        ## GET Y MINOR TICKS
        self.minor_yticks = np.concatenate(( self.major_yticks - 0.5*y_tick, 
                                np.array([ self.major_yticks[-1] + 0.5*y_tick ]) ))
        ## GET CBAR TICK WIDTH
        c_tick = 0.4
        ## GET CBAR MAJOR TICKS
        self.major_cticks = np.arange( 0.8, 2.4, c_tick )
        ## GET CBAR MINOR TICKS
        self.minor_cticks = np.concatenate(( self.major_cticks - 0.5*c_tick, 
                                np.array([ self.major_cticks[-1] + 0.5*c_tick ]) ))

    ## WC HEIGHT DEFAULTS
    def add_voronoi_defaults( self ):
        """Method adding wc height variance map defaults"""
        ## ADD X LABEL
        self.xlabel = "x (nm)"
        ## ADD Y LABEL
        self.ylabel = "y (nm)"
        ## GET X TICK WIDTH
        x_tick = 1
        ## GET X MAJOR TICKS
        self.major_xticks = np.arange( 0, 6, x_tick )
        ## GET X MINOR TICKS
        self.minor_xticks = np.concatenate(( self.major_xticks - 0.5*x_tick, 
                                np.array([ self.major_xticks[-1] + 0.5*x_tick ]) ))
        ## GET Y TICK WIDTH
        y_tick = 1
        ## GET Y MAJOR TICKS
        self.major_yticks = np.arange( 0, 7, y_tick )
        ## GET Y MINOR TICKS
        self.minor_yticks = np.concatenate(( self.major_yticks - 0.5*y_tick, 
                                np.array([ self.major_yticks[-1] + 0.5*y_tick ]) ))
            
## DEFAULTS FOR UMBRELLA SAMPLING
class INDUSDefaults:
    """Class containing default plotting variables for INDUS"""
    def __init__( self, plot_type ):
        ## IF PLOT TYPE IS HISTOGRAM
        if plot_type == "indus_histograms":
            self.add_histogram_defaults()
        elif plot_type == "indus_convergence_time":
            self.add_convergence_defaults()
        elif plot_type == "indus_equil_time":
            self.add_equilibration_defaults()
        elif plot_type == "indus_pN_distribution":
            self.add_pN_distribution_defaults()
        elif plot_type == "indus_hydration_fe":
            self.add_hydration_fe_defaults()
            
    ## INDUS HISTOGRAM DEFAULTS
    def add_histogram_defaults( self ):
        """Method adding histogram defaults"""
        ## ADD X LABEL
        self.xlabel = "Num. water"
        ## ADD Y LABEL
        self.ylabel = "Prob. Dist. Function"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 65.0, 5.0 )
        ## ADD Y TICKS
        self.yticks = np.arange( 0.0, 1.6, 0.2 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = None
        
    ## INDUS CONVERGENCE DEFAULTS
    def add_convergence_defaults( self ):
        """Method adding convergence time defaults"""
        ## ADD X LABEL
        self.xlabel = "Simulation time (ps)"
        ## ADD Y LABEL
        self.ylabel = r"$\mu_{\nu}$ (kT)"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 3500.0, 500.0 )
        ## ADD Y TICKS
        self.yticks = np.arange( 100.0, 106.0, 1.0 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## INDUS EQUILIBRATION DEFAULTS
    def add_equilibration_defaults( self ):
        """Method adding equilibration time defaults"""
        ## ADD X LABEL
        self.xlabel = "Initial frame (ps)"
        ## ADD Y LABEL
        self.ylabel = r"$\mu_{\nu}$ (kT)"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 2500.0, 500.0 )
        ## ADD Y TICKS
        self.yticks = np.arange( 100.0, 106.0, 1.0 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## INDUS P(N) DISTRIBUTION
    def add_pN_distribution_defaults( self ):
        """Method adding p(N) distribution defaults"""
        ## ADD X LABEL
        self.xlabel = "Num. water"
        ## ADD Y LABEL
        self.ylabel = r"$ln( \it{p}_{\nu}(N) )$"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 60.0, 5.0 )
        ## ADD Y TICKS
        self.yticks = np.arange( -120.0, 0.0, 20.0 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
        
    ## INDUS HYDRATION FE DEFAULTS
    def add_hydration_fe_defaults( self ):
        """Method adding hydration fe defaults"""
        ## ADD X LABEL
        self.xlabel = "Fraction"
        ## ADD Y LABEL
        self.ylabel = r"$\mu_{\nu}$ (kT)"
        ## ADD X TICKS
        self.xticks = np.arange( 0.0, 1.1, 0.1 )
        ## ADD Y TICKS
        self.yticks = np.arange( 30, 120.0, 10.0 )
        ## ADD NCOL TO LEGEND
        self.ncol_legend = 1
    