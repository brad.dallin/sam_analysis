"""
jacs_si.py
This script contains the class which constructs the plotting tool object
when adding an new tool must add a builder object for that tool. Follow the example
of one of the classed under plotting classes

CREATED ON: 12/14/2020

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:

"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS
import os
## IMPORT NUMPY
import numpy as np
## IMPORT MATPLOTLIB PYPLOT
import matplotlib.pyplot as plt

## FUNCTION TO SAVE AND LOAD PICKLE FILES
from sam_analysis.core.pickles import load_pkl
## IMPORT LINE PLOT FUNCTION
from sam_analysis.plotting.plot_types import plot_line, plot_points
## IMPORT PLOT STYLES
from sam_analysis.plotting.plot_styles import plot_style

##############################################################################
## FUNCTIONS & CLASSES
##############################################################################
## JACS SI CLASS
class JacsSi:
    """Class object to plot SI figures for JACS jornals"""
    
    def __init__( self ):
        """Initialize by resetting plotting parameters"""   
        ## SET FONT PARAMETERS
        plt.rcParams["font.sans-serif"]  = "Arial"
        plt.rcParams["font.family"]      = "sans-serif"
        plt.rcParams["font.size"]        = 8.0
        
        ## SET GRID PARAMETERS
        plt.rcParams["axes.grid"]        = True
        plt.rcParams["grid.linestyle"]   = ":"
        plt.rcParams["grid.linewidth"]   = "0.5"
        plt.rcParams["grid.color"]       = "grey"
        
        ## SET LEGEND PARAMETERS
        plt.rcParams["legend.loc"]       = "best"
        plt.rcParams["legend.fancybox"]  = True
        plt.rcParams["legend.facecolor"] = "white"
        plt.rcParams["legend.edgecolor"] = "black"
        
        ## SET FIGURE DIMENSIONS
        self.width  = 3.33
        self.height = 0.75 * self.width

    ## INSTANCE PREPARES SUBPLOT
    def prepare_plot( self, data_type, plot_type ):
        """Create subplots"""      
        ## INITIALIZE SUBPLOTS
        fig, ax = plt.subplots( 1, 1 )
        fig.subplots_adjust( left = 0.20, bottom = 0.18, right = 0.98, top = 0.98 )
        fig.set_size_inches( self.width, self.height )
        
        ## ADD STYLE TO PLOT
        ax = plot_style( ax,
                         x_axis_type = plot_type,
                         y_axis_type = data_type,
                         add_xlabel  = True,
                         add_ylabel  = True )
        
        return fig, ax
                    
    ## INSTANCE ADDS DATA TO SUBPLOTS
    def add_data( self, ax, plot_data ):
        """Function plotting data on subplots"""
        ## EXTRACT DATA
        x_data  = plot_data["x"]
        y_data  = plot_data["y"]
        y_err   = plot_data["yerr"]
        colors  = plot_data["colors"]
        markers = plot_data["markers"]
        labels  = plot_data["labels"]
        
        ## PLOT DATA
        ax = plot_points( ax        = ax, 
                          x_data    = x_data,
                          y_data    = y_data,
                          y_err     = y_err,
                          colors    = colors,
                          markers   = markers,
#                          linestyle = "-",
                          labels    = labels )
        
        ## ADD LEGEND
        ax.legend( loc           = 'upper left',
                   ncol          = 1,
                   fontsize      = 8,
                   labelspacing  = 0.2,
                   columnspacing = 1.0,
                   borderpad     = 0.4,
                   handlelength  = 1.0, )
        
        ## RETURN AXES
        return ax
    
    ## INSTANCE TO SAVE PLOTS
    def save_plot( self, fig, path_fig ):        
        ## SAVE FIG AS PNG AND SVG
        print( "FIGURE SAVED TO: %s" % path_fig )
        png = path_fig + ".png"
        svg = path_fig + ".svg"
        fig.savefig( png, dpi = 300, facecolor = 'w', edgecolor = 'w' )
        fig.savefig( svg, dpi = 300, facecolor = 'w', edgecolor = 'w' )

## CHARGE SCALED LABELS
CS_LABELS = {
              ## CHARGE SCALED NH2
              "NH2" :
              [ "CS0.0NH2", "CS0.1NH2", "CS0.2NH2", "CS0.3NH2", "CS0.4NH2", "CS0.5NH2",
                "CS0.6NH2", "CS0.7NH2", "CS0.8NH2", "CS0.9NH2", "NH2", ],
              ## CHARGE SCALED CONH2
              "CONH2" :
              [ "CS0.0CONH2", "CS0.1CONH2", "CS0.2CONH2", "CS0.3CONH2", "CS0.4CONH2", "CS0.5CONH2",
                "CS0.6CONH2", "CS0.7CONH2", "CS0.8CONH2", "CS0.9CONH2", "CONH2", ],
              ## CHARGE SCALED OH
              "OH" :
              [ "CS0.0OH", "CS0.1OH", "CS0.2OH", "CS0.3OH", "CS0.4OH", "CS0.5OH",
                "CS0.6OH", "CS0.7OH", "CS0.8OH", "CS0.9OH", "OH", ],
              }

## MIXED LABELS
MIX_LABELS = {
               ## MIXED NH2
               "NH2" :
               [ "CH3", "MIX25NH2", "MIX40NH2", "MIX50NH2", "MIX75NH2", "NH2", ],
               ## MIXED CONH2
               "CONH2" :
               [ "CH3", "MIX25CONH2", "MIX40CONH2", "MIX50CONH2", "MIX75CONH2", "CONH2", ],
               ## MIXED OH
               "OH" :
               [ "CH3", "MIX25OH", "MIX40OH", "MIX50OH", "MIX75OH", "OH", ],
               }

## SEPARATED LABELS
SEP_LABELS = {    
               ## SEPARATED NH2
               "NH2" :
               [ "CH3", "SEP25NH2", "SEP40NH2", "SEP50NH2", "SEP75NH2", "NH2", ],
               ## SEPARATED CONH2
               "CONH2" :
               [ "CH3", "SEP25CONH2", "SEP40CONH2", "SEP50CONH2", "SEP75CONH2", "CONH2", ],
               ## SEPARATED OH
               "OH" : 
               [ "CH3", "SEP25OH", "SEP40OH", "SEP50OH", "SEP75OH", "OH", ],
               }

## LINE LABELS
LINE_LABELS = { "charge_scaled"         : CS_LABELS,
                "mixed_composition"     : MIX_LABELS,
                "separated_composition" : SEP_LABELS,
                }
    
## XAXES
XAXES = { "charge_scaled"         : np.arange( 0.0, 1.1, 0.1 ),
          "mixed_composition"     : np.array([ 0.00, 0.25, 0.40, 0.50, 0.75, 1.00 ]),
          "separated_composition" : np.array([ 0.00, 0.25, 0.40, 0.50, 0.75, 1.00 ]),
          }

## PLOT TYPE LABELS
PLOT_TYPE_LABELS = { "hbonds_total"       : "hbonds_total",
                     "hbonds_sam_sam"     : "hbonds_sam_sam",
                     "hbonds_sam_water"   : "hbonds_sam_water_per_water",
                     "hbonds_water_water" : "hbonds_water_water",
                     "hfe_mu"             : "hfe_mu",
                     }

## LIST OF MARKER STYLES
MARKERS = { 
            "charge_scaled"         : "s",
            "mixed_composition"     : "^",
            "separated_composition" : "o",
            }

## LIST OF COLORS
COLORS = { 
           "CH3"   : "dimgrey",
           "NH2"   : "slateblue",
           "CONH2" : "darkseagreen",
           "OH"    : "tomato",
           "BULK"  : "black",
           }
        
#%%
##############################################################################
## TESTING SCRIPT
##############################################################################
if __name__ == "__main__":
    ## IMPORT CHECK SERVER PATH
    from sam_analysis.core.check_tools import check_server_path
    
    ## PLOT TYPE
    data_type  = "hfe_mu"
    plot_name  = "charge_scaled"
    plot_types = [ "charge_scaled" ]
    
    ## Y DATA TYPE
    yl = PLOT_TYPE_LABELS[data_type]
        
    ## DATA DIRECTORY
    data_dir = r"/mnt/r/python_projects/sam_analysis/sam_analysis/raw_data"
    
    ## SAM DIRECTORY
    data_pkl = r"indus_regression_data.pkl"
        
    ## CHECK PATH TO DATA
    path_data = os.path.join( data_dir, data_pkl )
    path_data = check_server_path( path_data )
        
    ## PATH FIG
    path_fig = r"water_{}_{}".format( data_type, plot_name )
    
    ## PATH TO FIGURE
    path_fig = os.path.join( data_dir, path_fig )
    path_fig = check_server_path( path_fig )
    ## LOAD DATA
    data = load_pkl( path_data )
        
    ## PLACE HOLDER FOR PANEL DATA
    line_data = { "x"       : [],
                  "y"       : [],
                  "yerr"    : [],
                  "colors"  : [],
                  "markers" : [],
                  "labels"  : [], }
    
    ## LOOP THROUGHT PLOT TYPES
    for plot_type in plot_types:    
        ## LOOP THROUGH LINE LABELS
        for key, labels in LINE_LABELS[plot_type].items():
            ## Y PLACEHOLDERS
            y_data = []
            y_err  = []
            ## LOOP THROUGH POINT LABELS
            for ll in labels:
                ## EXTRACT DATA OF INTEREST
                d = data[ll][data_type]
                y_data.append( np.mean( d ) )
                y_err.append( np.std( d ) )
                
            ## UPDATE LINE DATA
            x_data = XAXES[plot_type]
            line_data["x"].append( x_data )
            line_data["y"].append( y_data )
            line_data["yerr"].append( y_err )
            line_data["colors"].append( COLORS[key] )
            line_data["markers"].append( MARKERS[plot_type] )
            line_data["labels"].append( key )
    
    ## INITIALIZE PLOTS
    plot_obj = JacsSi()
    
    ## CREATE SUBPLOTS
    fig, ax = plot_obj.prepare_plot( data_type, plot_type )
    
    ## PLOT DATA
    ax = plot_obj.add_data( ax, line_data )
    
    ## SAVE FIG
    plot_obj.save_plot( fig, path_fig )
    