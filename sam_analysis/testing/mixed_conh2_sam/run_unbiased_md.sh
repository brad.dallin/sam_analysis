#!/bin/bash

# gather environment variables
simName=sam
simTime=10
ensemble=nvt
simTemperature=300
forcefield=charmm36-jul2017.ff
# Technical flags
num_mpi=4 # number of mpi threads
num_omp=7  # number of open mp threads
export OMP_NUM_THREADS=$num_omp
ffName=$( echo ${forcefield%-*} | tr [:lower:] [:upper:] )

# Input/Output folders
inputFolder=input_files   # includes topology, mdp files, itp files, etc. 
outputFolder=output_files # includes final trajectory files, analysis files, etc.

# source indus enabled gromacs (gmx v2016.6, plumed v2.5.1 w/ indus patch)
source "$HOME/.bashrc"
load_gmx_2016-6_thread

# run md simulation
echo "Running $ensemble simulation"
# ensure we are in the proper directory
echo "  Copying $inputFolder/ to $PWD/"
sed -e s/SIMTEMP/$simTemperature/g $inputFolder/${ensemble}_equil.mdp > $PWD/${ensemble}_equil.mdp
sed -e s/SIMTEMP/$simTemperature/g $inputFolder/${ensemble}.mdp > $PWD/${ensemble}.mdp
sed -i s/SIMTIME/$simTime/g $PWD/${ensemble}.mdp
simSteps=$(awk "BEGIN{ print $simTime * 1000 / 0.002; exit}") # number of simulation steps (simtime (ps)/0.002 (ps))
sed -i s/NUMSTEPS/$simSteps/g $PWD/${ensemble}.mdp

cp $inputFolder/${simName}* $PWD/

if [ $ensemble == nvt ]; then
    sed -e s/SIMTEMP/$simTemperature/g $inputFolder/${ensemble}_equil_water.mdp > $PWD/${ensemble}_equil_water.mdp 
    gmx grompp -maxwarn 3 -f ${ensemble}_equil_water.mdp -o ${simName}_equil_water.tpr -c ${simName}.gro -p ${simName}.top
    gmx mdrun -ntmpi $num_mpi -ntomp $num_omp -deffnm ${simName}_equil_water

    gmx grompp -maxwarn 3 -f ${ensemble}_equil.mdp -o ${simName}_equil.tpr -c ${simName}_equil_water.gro -p ${simName}.top
else
    gmx grompp -maxwarn 3 -f ${ensemble}_equil.mdp -o ${simName}_equil.tpr -c ${simName}.gro -p ${simName}.top
fi

gmx mdrun -ntmpi $num_mpi -ntomp $num_omp -deffnm ${simName}_equil

gmx grompp -maxwarn 3 -f ${ensemble}.mdp -o ${simName}_prod.tpr -c ${simName}_equil.gro -p ${simName}.top
gmx mdrun -ntmpi $num_mpi -ntomp $num_omp -deffnm ${simName}_prod

# remove unnecessary files
rm *.mdp

echo "*** $ensemble simulation complete ***"
