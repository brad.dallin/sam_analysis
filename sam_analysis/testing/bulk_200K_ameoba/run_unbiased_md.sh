#!/bin/bash

source ~/.bashrc
load_tinker

####################### 
# Simulation variables
#######################
NVT_EQUIL=0
NPT_EQUIL=1
NVT_PROD=1

## GENERAL VARIABLES
timestep=1          # time step (fs)
sim_temp=150        # temp. (K)
sim_press=1         # press. (atm)

## EQUILIBRATION VARIABLES
equil_time=50      # equilibration time (ps)
equil_stride=1     # equilibration stride (ps)
equil_nsteps=$( awk "BEGIN { print $equil_time * 1000 / $timestep; exit}" )
equil_stride_step=$( awk "BEGIN { print $equil_stride * 1000 / $timestep; exit}" )

## PRODUCTION VARIABLES
sim_time=100        # simulation time (ps)
sim_stride=0.1      # simulation stride (ps)
sim_nsteps=$( awk "BEGIN { print $sim_time * 1000 / $timestep; exit}" )
sim_stride_step=$( awk "BEGIN { print $sim_stride * 1000 / $timestep; exit}" )

#################### 
# NVT equilibration
####################
if [ $NVT_EQUIL -eq 1 ]; then
    # clean directory 
    rm -rvf equil/

    ## COPY INIT FILES TO MIN FILES
    cp -v enrg_min/bulk_water_em.xyz bulk_water_nvt_equil.xyz
    cp -v enrg_min/bulk_water_init.key bulk_water_nvt_equil.key

    ## CREATE TINKER INPUT FILES
    # key file
    echo "openmp-threads 28          # No. of parallel CPU threads to use" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key
    echo "cutoff 9.0                 # Nonbonded interactions direct cutoff" >> bulk_water_nvt_equil.key
    echo "ewald                      # Switch on PME" >> bulk_water_nvt_equil.key
    echo "ewald-cutoff 7.0           # PME real space cutoff (overrides the 9A above)" >> bulk_water_nvt_equil.key
    echo "vdw-correction             # Switch on analytical long-range vdW correction" >> bulk_water_nvt_equil.key
    echo "polar-eps 0.00001          # Dipole convergence criterion in RMS Debye/atm" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key
    echo "neighbor-list              # Use pairwise neighbor list for calculating" >> bulk_water_nvt_equil.key
    echo "                           # nonbonded interactions (improves speed)" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key
    echo "printout $equil_stride_step      # Interval at which to print out energies" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key
    echo "thermostat berendsen       # Switch on Berendsen thermostat" >> bulk_water_nvt_equil.key
    echo "integrator verlet          # Use a velocity-Verlet integrator" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key
    echo "archive                    # Create a single trajectory file with all" >> bulk_water_nvt_equil.key
    echo "                           # MD snapshots concatenated in sequence" >> bulk_water_nvt_equil.key
    echo "" >> bulk_water_nvt_equil.key

    # run nvt
    # dynamic.x sample.xyz -k sample.key <nsteps> <timestep (fs)> <out_equil_stride (ps)> <ensemble (nvt=2, npt=4) <temp (K)> 
    echo "cmd: dynamic.x bulk_water_nvt_equil.xyz -k bulk_water_nvt_equil.key $equil_nsteps $timestep $equil_stride 2 $sim_temp > bulk_water_nvt_equil.out"
    time dynamic.x bulk_water_nvt_equil.xyz -k bulk_water_nvt_equil.key $equil_nsteps $timestep $equil_stride 2 $sim_temp > bulk_water_nvt_equil.out

    # move nvt files to new folder
    mkdir -p equil/
    mv -v bulk_water_nvt_equil.* equil/
fi

#################### 
# NPT equilibration
####################
if [ $NPT_EQUIL -eq 1 ]; then
    # clean directory 
    rm -vf equil/*npt_equil*

    ## EXTRACT FINAL FRAME FROM TRAJECTORY
    last_frame=$( awk "BEGIN { print $equil_time / $equil_stride; exit}" )
    echo "2" > archive.dat
    echo "$last_frame $last_frame 1" >> archive.dat
    echo "" >> archive.dat
    echo "" >> archive.dat
    archive.x equil/bulk_water_nvt_equil.arc < archive.dat
    rm -v archive.dat
    mv bulk_water_nvt_equil.0${last_frame} bulk_water_npt_equil.xyz

    # key file
    cp -v enrg_min/bulk_water_init.key bulk_water_npt_equil.key
    echo "openmp-threads 28          # No. of parallel CPU threads to use" >> bulk_water_npt_equil.key
    echo "" >> bulk_water_npt_equil.key
    echo "cutoff 9.0                 # Nonbonded interactions direct cutoff" >> bulk_water_npt_equil.key
    echo "ewald                      # Switch on PME" >> bulk_water_npt_equil.key
    echo "ewald-cutoff 7.0           # PME real space cutoff (overrides the 9A above)" >> bulk_water_npt_equil.key
    echo "vdw-correction             # Switch on analytical long-range vdW correction" >> bulk_water_npt_equil.key
    echo "polar-eps 0.00001          # Dipole convergence criterion in RMS Debye/atm" >> bulk_water_npt_equil.key
    echo "" >> bulk_water_npt_equil.key
    echo "neighbor-list              # Use pairwise neighbor list for calculating" >> bulk_water_npt_equil.key
    echo "                           # nonbonded interactions (improves speed)" >> bulk_water_npt_equil.key
    echo "" >> bulk_water_npt_equil.key
    echo "printout $equil_stride_step               # Interval at which to print out energies" >> bulk_water_npt_equil.key
    echo "" >> bulk_water_npt_equil.key
    echo "thermostat berendsen       # Switch on Berendsen thermostat" >> bulk_water_npt_equil.key
    echo "integrator verlet          # Use a velocity-Verlet integrator" >> bulk_water_npt_equil.key
    echo "barostat berendsen         # Switch on Berendsen barostat" >> bulk_water_npt_equil.key
    echo "tau-pressure 1.0           # Set pressure coupling time to 1.0 ps" >> bulk_water_npt_equil.key
    echo "" >> bulk_water_npt_equil.key
    echo "                           # MD snapshots concatenated in sequence" >> bulk_water_npt_equil.key
    
    # run npt
    # dynamic.x sample.xyz -k sample.key <nsteps> <timestep (fs)> <out_equil_stride (ps)> <ensemble (nvt=2, npt=4) <temp (K)> <press (atm)>
    echo "cmd: dynamic.x bulk_water_npt_equil.xyz -k bulk_water_npt_equil.key $equil_nsteps $timestep $equil_stride 4 $sim_temp $sim_press > bulk_water_npt_equil.out"
    time dynamic.x bulk_water_npt_equil.xyz -k bulk_water_npt_equil.key $equil_nsteps $timestep $equil_stride 4 $sim_temp $sim_press > bulk_water_npt_equil.out

    # move npt files to equil
    mkdir -p equil/
    mv -v bulk_water_npt_equil.* equil/
fi

################ 
# NVT production
################
if [ $NVT_PROD -eq 1 ]; then
    # clean directory 
    rm -vf prod/

    ## EXTRACT FINAL FRAME FROM NPT TRAJECTORY
    last_frame=$( awk "BEGIN { print $equil_time / $equil_stride; exit}" )
    echo "2" > archive.dat
    echo "$last_frame $last_frame 1" >> archive.dat
    echo "" >> archive.dat
    echo "" >> archive.dat
    archive.x equil/bulk_water_npt_equil.arc < archive.dat
    rm -v archive.dat
    mv bulk_water_npt_equil.0${last_frame} bulk_water_nvt_prod.xyz
    
    ## CREATE TINKER INPUT FILES
    # key file
    cp -v enrg_min/bulk_water_init.key bulk_water_nvt_prod.key
    echo "openmp-threads 28          # No. of parallel CPU threads to use" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key
    echo "cutoff 9.0                 # Nonbonded interactions direct cutoff" >> bulk_water_nvt_prod.key
    echo "ewald                      # Switch on PME" >> bulk_water_nvt_prod.key
    echo "ewald-cutoff 7.0           # PME real space cutoff (overrides the 9A above)" >> bulk_water_nvt_prod.key
    echo "vdw-correction             # Switch on analytical long-range vdW correction" >> bulk_water_nvt_prod.key
    echo "polar-eps 0.00001          # Dipole convergence criterion in RMS Debye/atm" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key
    echo "neighbor-list              # Use pairwise neighbor list for calculating" >> bulk_water_nvt_prod.key
    echo "                           # nonbonded interactions (improves speed)" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key
    echo "printout $sim_stride_step      # Interval at which to print out energies" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key
    echo "thermostat berendsen        # Switch on Berendsen thermostat" >> bulk_water_nvt_prod.key
    echo "integrator verlet          # Use a velocity-Verlet integrator" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key
    echo "archive                    # Create a single trajectory file with all" >> bulk_water_nvt_prod.key
    echo "                           # MD snapshots concatenated in sequence" >> bulk_water_nvt_prod.key
    echo "" >> bulk_water_nvt_prod.key

    # run nvt
    # dynamic.x sample.xyz -k sample.key <nsteps> <timestep (fs)> <out_stride (ps)> <ensemble (nvt=2, npt=4) <temp (K)> 
    echo "cmd: dynamic.x bulk_water_nvt_prod.xyz -k bulk_water_nvt_prod.key $sim_nsteps $timestep $sim_stride 2 $sim_temp > bulk_water_nvt_prod.out"
    time dynamic.x bulk_water_nvt_prod.xyz -k bulk_water_nvt_prod.key $sim_nsteps $timestep $sim_stride 2 $sim_temp > bulk_water_nvt_prod.out

    # move nvt files to new folder
    mkdir -p prod/
    mv -v bulk_water_nvt_prod.* prod/
fi