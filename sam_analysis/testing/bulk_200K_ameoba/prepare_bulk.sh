#!/bin/bash

source ~/.bashrc
load_tinker

# from amoebapro13.prm
oxygen_type="247"
hydrogen_type="248"

####################
# PREPARE WATER BOX
####################
## CREATE TINKER INPUT FILES
# clean up directory
rm -vf *.xyz* *.pdb* *.key* *.out

# key file
> bulk_water_init.key
echo "parameters /home/bdallin/simulations/polar_sams/unbiased/bulk_200K_amoeba/amoebapro13.prm" >> bulk_water_init.key
# echo "parameters /home/bdallin/simulations/surfactants/amoeba/bulk_water/water14.prm" >> bulk_water_init.key
echo "" >> bulk_water_init.key
echo "a-axis 24.662" >> bulk_water_init.key
echo "b-axis 24.662" >> bulk_water_init.key
echo "c-axis 24.662" >> bulk_water_init.key
echo "" >> bulk_water_init.key

## COPY WATER BOX
cp -v ${HOME}/local_installs/tinker/example/waterbox.xyz ./bulk_water.xyz

## UPDATE ATOM TYPES TO PROPER FF
xyzedit.x bulk_water.xyz -k bulk_water_init.key << INPUT
6
1,247
6
2,248
17
24.662,24.662,24.662

INPUT
# rename output to init input
mv -v bulk_water.xyz_2 bulk_water_init.xyz

###################### 
# ENERGY MINIMIZATION
######################
## COPY INIT FILES TO MIN FILES
cp -v bulk_water_init.key bulk_water_em.key

## CREATE TINKER INPUT FILES
# key file
echo "openmp-threads 24          # No. of parallel CPU threads to use" >> bulk_water_em.key
echo "" >> bulk_water_em.key
echo "cutoff 9.0                 # Nonbonded interactions direct cutoff" >> bulk_water_em.key
echo "ewald                      # Switch on PME" >> bulk_water_em.key
echo "ewald-cutoff 7.0           # PME real space cutoff (overrides the 9A above)" >> bulk_water_em.key
echo "vdw-correction             # Switch on analytical long-range vdW correction" >> bulk_water_em.key
echo "polar-eps 0.00001          # Dipole convergence criterion in RMS Debye/atm" >> bulk_water_em.key
echo "" >> bulk_water_em.key
echo "neighbor-list              # Use pairwise neighbor list for calculating" >> bulk_water_em.key
echo "                           # nonbonded interactions (improves speed)" >> bulk_water_em.key
echo "" >> bulk_water_em.key
echo "maxiter 10000              # Maximum number of minimisation steps" >> bulk_water_em.key
echo "steepest-descent           # Use the SD minimisation algorithm" >> bulk_water_em.key
echo "" >> bulk_water_em.key
echo "printout 100               # Interval at which to print out energies" >> bulk_water_em.key

# enegy minimize
time minimize.x bulk_water_init.xyz -k bulk_water_em.key 0.01 > bulk_water_em.out
mv -v bulk_water_init.xyz_2 bulk_water_em.xyz

# move minimization files to new folder
mkdir -p enrg_em
mv -v bulk_water* enrg_min/

