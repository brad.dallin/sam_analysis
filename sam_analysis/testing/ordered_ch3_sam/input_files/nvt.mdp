title                   = NVT equilibration @ SIMTEMP K
; Run parameters
integrator              = md               ; leap-frog integrator
nsteps                  = NUMSTEPS          ; SIMTIME ns
dt                      = 0.002            ; 2 fs
; Output control
nstxout                 = 0                ; save every 0 ps
nstvout                 = 0                ; save every 0 ps
nstxout-compressed      = 500              ; save every 1 ps
nstenergy               = 100              ; save energies every 0.2 ps
nstlog                  = 100              ; update log file every 0.2 ps
; Bond parameters
continuation            = yes              ; first dynamics run
constraint_algorithm    = lincs            ; holonomic constraints 
constraints             = h-bonds          ; bonds involving H are constrained
lincs_iter              = 1                ; accuracy of LINCS
lincs_order             = 4                ; also related to accuracy
; Nonbonded settings 
cutoff-scheme           = Verlet           ; Buffered neighbor searching
ns_type                 = grid             ; search neighboring grid cells
nstlist                 = 10               ; 20 fs, largely irrelevant with Verlet
rcoulomb                = 1.2              ; short-range electrostatic cutoff (in nm)
rvdw                    = 1.2              ; short-range van der Waals cutoff (in nm)
rvdw-switch             = 1.0              ; when to start switching off force
vdwtype                 = Cutoff	  
vdw-modifier            = Potential-switch ; smoothing function based on Bockmann
DispCorr                = EnerPres         ; account for cut-off vdW scheme
; Electrostatics
coulombtype             = PME              ; Particle Mesh Ewald for long-range electrostatics
pme_order               = 4                ; cubic interpolation
fourierspacing          = 0.16             ; grid spacing for FFT
; Temperature coupling is on
tcoupl                  = v-rescale	       ; modified Berendsen thermostat
tc-grps                 = System           ; coupling groups
tau_t                   = 0.1              ; time constant, in ps
ref_t                   = SIMTEMP          ; reference temperature, one for each group, in K
; Pressure coupling is off
pcoupl                  = no               ; no pressure coupling in NVT
; Periodic boundary conditions
pbc                     = xyz              ; 3-D PBC
; Velocity generation
gen_vel                 = no               ; assign velocities from Maxwell distribution
gen_temp                = SIMTEMP          ; temperature for Maxwell distribution
gen_seed                = -1               ; generate a random seed
comm-mode               = Linear           ; remove COM motion; possibly freeze Au atoms?
nstcomm                 = 10               ; remove every step
nstcalcenergy           = 5                ; calculate energy every step
