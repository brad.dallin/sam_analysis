#!/bin/bash

## READ IN DIRECTORY NAME
working_dir=$1

## REMOVE WHAM DIR
rm -rv $working_dir/wham

## MOVE TO UMBRELLA DIRECTORY
cd $working_dir/umbrella

## LOOP THROUGH INDUS WINDOWS
for dir in indus_*; do
    ## MOVE INTO DIR
    cd $dir

    ## REMOVE FF DIR
    rm -rv charmm36-jul2017.ff

    ## REMOVE OUT GRO
    rm -v sam_indus_*.gro

    ## REMOVE FILES
    find . -type f -not -name 'histo_*'         \
                   -not -name 'plumed_indus_*'  \
                   -not -name 'sam_*.gro'       \
                   -not -name 'water_num.*'     \
                   -not -name 'sam_indus_*.tpr' \
                   -not -name 'sam.top'         \
                   -not -name 'indus.input'     \
                   -not -name 'us_prod.mdp'     \
                   -delete

    ## MOVE OUT OF DIR
    cd ../
done
