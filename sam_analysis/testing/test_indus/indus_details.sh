#!/bin/bash

# List of simulation details to be specified for an INDUS simulation

# flags for what aspect to run - if 1, run it
RUN_EQUIL=0
CALC_DIMS=0
RUN_INIT=1
RUN_UMBRELLA=1

# specify number of waters in slab (-1: number of waters determined by slab size)
NUM_WATER=-1

# specify slab dimensions (default: 2.0x2.0x0.3 nm^3), if NUM_WATER > 0 only x and y needed
SLAB_DIMENSIONS=2.0,2.0,0.3

# weak spring parameters, for many water molecules
SPRING_WEAK=2.0      # spring constant used in PLUMED input files (UNITS KJ/MOL/N^2)
NUM_INCR_WEAK=5

# strong spring parameters, for few water molecules
SPRING_STRONG=8.5   # spring constant used in PLUMED input files (UNITS KJ/MOL/N^2)
START_NUM_STRONG=8
NUM_INCR_STRONG=2
