#!/bin/bash

# gather environment variables
sim_name=sam
pull_rate=1000
sim_time=5
sim_temperature=300
forcefield=charmm36-jul2017.ff
# Technical flags
num_mpi=16  # number of mpi threads
num_omp=3  # number of open mp threads
export OMP_NUM_THREADS=$num_omp

# set ensemble to nvt (must be nvt for water with free interface)
ensemble=nvt
bias_const="100.0" # kj/mol/N^2

# source indus enabled gromacs (gmx v2016.6, plumed v2.5.1 w/ indus patch)
source "$HOME/.bash_aliases"
load_gmx_2016-6_plumed_2-5-1

# set path variables
source $HOME/indus/indus_rc.sh
source ./indus_details.sh
wd="$PWD"

# analysis paths and files
path_analysis="$PATH_PREP_SCRIPTS/analysis_files"
py_wc="$HOME/bin/python_modules/MDDescriptors/surface/generate_wc_grid.py"
py_position="$path_analysis/indus_cavity_position.py"
py_truncate="$path_analysis/truncate_indus_data.py"
py_histogram="$path_analysis/combine_histo.py"
py_analysis="$path_analysis/analyze_indus_wham.py"
wham_analyis="$path_analysis/grossfield_wham/execute_wham_2D"

# Input/Output folders
input_folder=input_files   # includes topology, mdp files, itp files, etc. 
output_folder=output_files # includes final trajectory files, analysis files, etc.

# grossfield wham uses kcal/mol, can be changed to kJ/mol but must be recompiled
# see http://membrane.urmc.rochester.edu/sites/default/files/wham/doc.pdf
# easier to just convert, where PLUMED, GROMACS, and INDUS use kJ/mol
spring_weak_kcal=$(awk "BEGIN{ print 0.239006 * $SPRING_WEAK; exit}")
spring_strong_kcal=$(awk "BEGIN{ print 0.239006 * $SPRING_STRONG; exit}")

# run nvt equil simulation
if [ $RUN_EQUIL -eq 1 ]; then
    echo ""
    echo "Running $ensemble equilibration simulation"
    # ensure we are in the proper directory
    cd $wd/
    echo "  Creating $wd/equil"
    echo "  Copying $wd/$forcefield/ to $wd/$ensemble"
    echo "  Copying $wd/$input_folder/${ensemble}_equil.mdp to $wd/equil/${ensemble}_equil.mdp"
    mkdir -p equil
    cp -r $forcefield/ equil/
    sed -e s/SIMTEMP/$sim_temperature/g $input_folder/${ensemble}_equil_water.mdp > equil/${ensemble}_equil_water.mdp 
    sed -e s/SIMTEMP/$sim_temperature/g $input_folder/${ensemble}_equil.mdp > equil/${ensemble}_equil.mdp

    cp $input_folder/${sim_name}* equil/
    cd equil/

    gmx_mpi grompp -f ${ensemble}_equil_water.mdp -o ${sim_name}_equil_water.tpr -c ${sim_name}.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_equil_water

    gmx_mpi grompp -f ${ensemble}_equil.mdp -o ${sim_name}_equil.tpr -c ${sim_name}_equil_water.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -v -ntomp $num_omp -deffnm ${sim_name}_equil

    # remove unnecessary files
    rm -f \#*
    cd ../
fi

echo ""
echo ""
# begin indus simulation
echo "Beginning INDUS simulation"

# determine placement of cavity on SAM surface
if [[ ! -e $wd/equil/${sim_name}_equil.gro && ! -e $wd/equil/${sim_name}_equil.xtc ]]; then
    echo ""
    echo "  ERROR! Equilibration simulation not completed"
    echo ""; exit
else
    if [ $CALC_DIMS -eq 1 ]; then
        gmx_mpi make_ndx -f equil/${sim_name}_equil.gro -o equil/index.ndx << INPUT
a OW
q
INPUT
        gmx_mpi trjconv -f equil/${sim_name}_equil.xtc -o equil/${sim_name}_wc.xtc -s equil/${sim_name}_equil.tpr -n equil/index.ndx -b 4000 -pbc whole << INPUT
OW
INPUT
        gmx_mpi trjconv -f equil/${sim_name}_equil.gro -o equil/${sim_name}_wc.gro -s equil/${sim_name}_equil.tpr -n equil/index.ndx -pbc whole << INPUT
OW
INPUT

        python3 $py_wc --path "$wd/equil/" --gro "${sim_name}_wc.gro" --xtc "${sim_name}_wc.xtc" --output_file "$wd/$output_folder/" --output_prefix "${sim_name}_equil" --n_procs "20" --mesh "0.1,0.1,0.1" --alpha "0.24" --contour "16.0"
        python3 $py_position --wc "${sim_name}_equil_willard_chandler.dat" --wc_path "$wd/$output_folder/" --gro "${sim_name}_equil_whole.gro" --xtc "${sim_name}_equil_whole.xtc" --traj_path "$wd/equil/" --dimensions "2.0,2.0,0.3" --num_waters "-1"
    fi

    cavity_position=$(tail -n 1 $wd/$output_folder/cavity_coordinates.csv)
    cavity_position=${cavity_position##*: }
    x_center=$( echo ${cavity_position%%,*} | bc)
    x_center=$(printf "%.3f" $x_center)
    y_center=${cavity_position%,*}
    y_center=$( echo ${y_center#*,} | bc)
    y_center=$(printf "%.3f" $y_center)
    z_center=$( echo ${cavity_position##*,} | bc)
    z_center=$(printf "%.3f" $z_center)
    echo "  centering INDUS cavity at: $cavity_position"

    cavity_dimensions=$(tail -n 1 $wd/$output_folder/cavity_dimensions.csv)
    cavity_dimensions=${cavity_dimensions##*: }
    x_dimension=${cavity_dimensions%%,*}
    x_dimension=$( awk "BEGIN{ print $x_dimension * 0.5; exit}" )
    x_dimension=$(printf "%.3f" $x_dimension)
    y_dimension=${cavity_dimensions%,*}
    y_dimension=${y_dimension#*,}
    y_dimension=$( awk "BEGIN{ print $y_dimension * 0.5; exit}" )
    y_dimension=$(printf "%.3f" $y_dimension)
    z_dimension=${cavity_dimensions##*,}
    z_dimension=$( awk "BEGIN{ print $z_dimension * 0.5; exit}" )
    z_dimension=$(printf "%.3f" $z_dimension)
    echo " INDUS cavity dimensions: x: -$x_dimension,$x_dimension  y: -$y_dimension,$y_dimension  z: -$z_dimension,$z_dimension"

    x_min=$( awk "BEGIN{ print $x_center - $x_dimension; exit}" )
    x_max=$( awk "BEGIN{ print $x_center + $x_dimension; exit}" )
    y_min=$( awk "BEGIN{ print $y_center - $y_dimension; exit}" )
    y_max=$( awk "BEGIN{ print $y_center + $y_dimension; exit}" )
    z_min=$( awk "BEGIN{ print $z_center - $z_dimension; exit}" )
    z_max=$( awk "BEGIN{ print $z_center + $z_dimension; exit}" )
    echo "  INDUS cavity ranges: x: $x_min $x_max  y: $y_min $y_max  z: $z_min $z_max"

    start_num_weak=$(tail -n 1 $wd/$output_folder/num_waters.csv)
    start_num_weak=${start_num_weak##*: }
    echo "  number of waters in INDUS cavity: $start_num_weak"

    num_windows_weak=$( awk "BEGIN{ print ($start_num_weak - $START_NUM_STRONG - $NUM_INCR_STRONG) / $NUM_INCR_WEAK + 1; exit}" )
    num_windows_strong=$( awk "BEGIN{ print $START_NUM_STRONG / $NUM_INCR_STRONG + 1; exit}" )
fi

if [ $RUN_INIT -eq 1 ]; then
    mkdir -p init
    # copy in needed files
    cp -rv $wd/$forcefield init/
    cp -v $wd/equil/${sim_name}_equil.gro init/
    cp -v $wd/$input_folder/${sim_name}.top init/

    # launch
    cd init

    # Initially set N waters inside cavity
    echo "Relaxing cavity to initial N"
    cp -v $wd/$input_folder/nvt_cavity_relax.mdp ./nvt_cavity_relax.mdp
    sed -i s/SIMTEMP/$sim_temperature/g ./nvt_cavity_relax.mdp

    cp -v $wd/$input_folder/indus.input ./indus.input
    sed -i s/GRONAME/"${sim_name}_equil.gro"/g indus.input
    sed -i s/XRANGE/"$x_min $x_max"/g indus.input
    sed -i s/YRANGE/"$y_min $y_max"/g indus.input
    sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

    cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_relax.dat
    sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_relax.dat
    sed -i s/BIAS/$SPRING_STRONG/g ./plumed_indus_relax.dat
    sed -i s/NUMWATER/$start_num_weak/g plumed_indus_relax.dat
    sed -i s/MAXWATER/$start_num_weak/g plumed_indus_relax.dat
    sed -i s/DUMPSTEP/'99980'/g plumed_indus_relax.dat

    # first adjust box to start num using normal restraint
    gmx_mpi grompp -f nvt_cavity_relax.mdp -o ${sim_name}_relax -c ${sim_name}_equil.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_relax -plumed plumed_indus_relax.dat

    echo "Generating initial INDUS configurations from steered MD"
    # calculating pulling windows and pulling time
    pull_windows=$(echo "$start_num_weak + 1" | bc)
    # Total number of steps to pull, rounded to nearest integer
    pull_steps=$(echo "(($pull_rate * $start_num_weak)+0.5)/1" | bc)
    # Total amount of time to pull, rounded to nearest integer
    pull_time=$(echo "$pull_steps / 500" | bc)

    cp -v $wd/$input_folder/nvt_steered.mdp ./nvt_steered.mdp
    # edit ndp inputs
    sed -i s/SIMTEMP/$sim_temperature/g ./nvt_steered.mdp
    sed -i s/PULLTIME/$pull_time/g nvt_steered.mdp
    sed -i s/PULLSTEPS/$pull_steps/g nvt_steered.mdp

    cp -v $wd/$input_folder/plumed_indus_steered.dat ./
    sed -i s/INDUSFILE/"indus.input"/g plumed_indus_steered.dat
    sed -i s/STARTNUM/$start_num_weak/g plumed_indus_steered.dat
    sed -i s/PULLSTEPS/$pull_steps/g plumed_indus_steered.dat
    sed -i s/BIAS/$bias_const/g plumed_indus_steered.dat 

    # now decrease box water as a function of time (steered MD)
    gmx_mpi grompp -f nvt_steered.mdp -o ${sim_name}_steered -c ${sim_name}_relax.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_steered -plumed plumed_indus_steered.dat

    # remove unnecessary files
    rm -f \#*
    rm -f bck.*
    rm -f analysis.*
    cd ../

    echo "Extracting initial INDUS configurations"
    mkdir -p extract_frames
    cd extract_frames/   

    # How often to extract frame from trajectory (for extract frames) - in ps, assuming 0.002 fs timestep
    pull_incr=$(echo "$pull_rate / 500" | bc)

    # iterate
    for (( i=0; i<$pull_windows; i++)); do

        time=$(($i * $pull_incr))
        water=$(echo "$start_num_weak - $i" | bc)

        gmx_mpi trjconv -f ../init/${sim_name}_steered.xtc -s ../init/${sim_name}_steered.tpr -dump ${time} -o ${sim_name}_${water}.gro << INPUT
SYSTEM
INPUT
    done
    cd ../
fi

if [ $RUN_UMBRELLA -eq 1 ]; then
    echo ""
    echo "Running INDUS simulations"
    mkdir -p umbrella
    cd umbrella/

    # iterate over each frame and run the corresponding job
    for (( i=0; i<$num_windows_weak; i++)); do
        water=$(echo "$start_num_weak - $i * $NUM_INCR_WEAK" | bc)

        mkdir -p indus_${water}
        cd indus_${water}

        # copy umbrella sampling file
        cp -rv $wd/$forcefield ./
        cp -v $wd/$input_folder/${sim_name}.top ./
        cp -v $wd/extract_frames/${sim_name}_${water}.gro ./

        sim_steps=$(awk "BEGIN{ print $sim_time * 1000 / 0.002; exit}") # number of simulation steps (sim_time (ps)/0.002 (ps))
        dump_step=$(( $sim_steps - 20 ))
        cp -v $wd/$input_folder/us_prod.mdp ./us_prod.mdp
        sed -i s/SIMTEMP/$sim_temperature/g ./us_prod.mdp
        sed -i s/NUMSTEPS/$sim_steps/g ./us_prod.mdp
        sed -i s/SIMTIME/$sim_time/g ./us_prod.mdp

        cp -v $wd/$input_folder/indus.input ./indus.input
        sed -i s/GRONAME/"${sim_name}_${water}.gro"/g indus.input
        sed -i s/XRANGE/"$x_min $x_max"/g indus.input
        sed -i s/YRANGE/"$y_min $y_max"/g indus.input
        sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

        cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_${water}.dat
        sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_${water}.dat
        sed -i s/BIAS/$SPRING_WEAK/g ./plumed_indus_${water}.dat
        sed -i s/NUMWATER/$water/g ./plumed_indus_${water}.dat
        sed -i s/MAXWATER/$start_num_weak/g ./plumed_indus_${water}.dat
        sed -i s/DUMPSTEP/$dump_step/g ./plumed_indus_${water}.dat

        # run grompp
        gmx_mpi grompp -f us_prod.mdp -o ${sim_name}_indus_${water} -c ${sim_name}_${water}.gro -p ${sim_name}.top
        mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_indus_${water} -plumed plumed_indus_${water}.dat
        
        # remove unnecessary files
        rm -f \#*
        rm -f bck.*
        rm -f analysis.*
        cd ../
    done


    # iterate over each frame and run the corresponding job
    for (( i=0; i<$num_windows_strong; i++)); do
        water=$(echo "$START_NUM_STRONG - $i * $NUM_INCR_STRONG" | bc)

        mkdir -p indus_${water}
        cd indus_${water}

        # copy umbrella sampling file
        cp -rv $wd/$forcefield ./
        cp -v $wd/$input_folder/${sim_name}.top ./
        cp -v $wd/extract_frames/${sim_name}_${water}.gro ./

        sim_steps=$(awk "BEGIN{ print $sim_time * 1000 / 0.002; exit}") # number of simulation steps (sim_time (ps)/0.002 (ps))
        dump_step=$(( $sim_steps - 20 ))
        cp -v $wd/$input_folder/us_prod.mdp ./us_prod.mdp
        sed -i s/SIMTEMP/$sim_temperature/g ./us_prod.mdp
        sed -i s/NUMSTEPS/$sim_steps/g ./us_prod.mdp
        sed -i s/SIMTIME/$sim_time/g ./us_prod.mdp

        cp -v $wd/$input_folder/indus.input ./indus.input
        sed -i s/GRONAME/"${sim_name}_${water}.gro"/g indus.input
        sed -i s/XRANGE/"$x_min $x_max"/g indus.input
        sed -i s/YRANGE/"$y_min $y_max"/g indus.input
        sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

        cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_${water}.dat
        sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_${water}.dat
        sed -i s/BIAS/$SPRING_STRONG/g ./plumed_indus_${water}.dat
        sed -i s/NUMWATER/$water/g ./plumed_indus_${water}.dat
        sed -i s/MAXWATER/$start_num_weak/g ./plumed_indus_${water}.dat
        sed -i s/DUMPSTEP/$dump_step/g ./plumed_indus_${water}.dat

        # run grompp
        gmx_mpi grompp -f us_prod.mdp -o ${sim_name}_indus_${water} -c ${sim_name}_${water}.gro -p ${sim_name}.top
        mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_indus_${water} -plumed plumed_indus_${water}.dat

        # remove unnecessary files
        rm -f \#*
        rm -f bck.*
        rm -f analysis.*
        cd ../
    done
    cd ../
fi
